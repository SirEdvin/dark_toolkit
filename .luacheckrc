globals = {
    "vector", "turtle", "gps", "sleep", "fs", "colors", "term",
    "shell", "keys", "parallel", "peripheral", "redstone", "http",
    "textutils", "rednet", "BUS", "printError",
}

globals["os"] = {
    fields = {"getComputerLabel", "pullEvent", "reboot", "queueEvent", "epoch"}
}
globals["settings"] ={
    fields = {"get"}
}

ignore = {"212"}
