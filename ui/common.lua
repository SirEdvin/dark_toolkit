local expect = require "cc.expect"

local core = require("ui.core")

local PseudoButton = core.UIComponent:new()
PseudoButton.text = ""
PseudoButton.baseColor = colors.lime
PseudoButton.disabledColor = colors.gray

function PseudoButton:new(text, baseColor, disabledColor, o)
    expect.expect(1, text, "string")
    expect.expect(2, baseColor, "number", "nil")
    expect.expect(3, disabledColor, "number", "nil")
    expect.expect(4, o, "table", "nil")

    o = o or core.UIComponent:new(o)
    setmetatable(o, self)
    self.__index = self
    o.text = text
    o.baseColor = baseColor or o.baseColor
    o.disabledColor = disabledColor or o.disabledColor
    return o
end

function PseudoButton:_selectColor()
    if self.enabled then
        return self.baseColor
    end
    return self.disabledColor
end

function PseudoButton:_draw(context)
    context.monitor.write('[')
    context:withColor(self:_selectColor(), context.monitor.write, self.text)
    context.monitor.write(']')
end

local HorizontalDivider = core.UIComponent:new()
HorizontalDivider.symbol = "\140"

function HorizontalDivider:new(symbol, o)
    expect.expect(1, symbol, "string", "nil")
    expect.expect(2, o, "table", "nil")

    o = o or core.UIComponent:new(o)
    setmetatable(o, self)
    self.__index = self
    o.symbol = symbol or o.symbol
    return o
end

function HorizontalDivider:_draw(context)
    local width, _ = context.monitor.getSize()
    context.monitor.write(string.rep("\140", width))
end


return {
    PseudoButton = PseudoButton,
    HorizontalDivider = HorizontalDivider,
}