local expect = require "cc.expect"

local UIEventRegistry = {
    CLICK = "CLICK"
}

local UIDrawContext = {
    monitor = nil,
    collapsed = true
}

function UIDrawContext:new(monitor, collapsed)
    expect.expect(1, monitor, "table")
    expect.expect(2, collapsed, "boolean", "nil")

    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.monitor = monitor
    o.collapsed = collapsed or o.collapsed
    return o
end

function UIDrawContext:slideDownAfter(func, ...)
    local startX, _ = self.monitor.getCursorPos()
    local funcResult = func(...)
    local _, curY = self.monitor.getCursorPos()
    self.monitor.setCursorPos(startX, curY + 1)
    return funcResult
end

function UIDrawContext:withColor(color, func, ...)
    local curColor = self.monitor.getTextColor()
    self.monitor.setTextColor(color)
    local funcResult = func(...)
    self.monitor.setTextColor(curColor)
    return funcResult
end

local UIComponent = {
    startX = 0, startY = 0, enabled = true,
    endX = 0, endY = 0, handlers = nil, monitorName = ""
}

function UIComponent:new(o)
    o = {} or o
    setmetatable(o, self)
    self.__index = self
    o.handlers = {}
    return o
end

function UIComponent:draw(context)
    self.monitorName = peripheral.getName(context.monitor)
    self.startX, self.startY = context.monitor.getCursorPos()
    self:_draw(context)
    self.endX, self.endY = context.monitor.getCursorPos()
end

function UIComponent:_draw(context)
    error("This function should be overwitten!")
end

function UIComponent:onClick(listener)
    self.handlers[UIEventRegistry.CLICK] = listener
end

function UIComponent:isInside(x, y)
    if self.startX <= x and self.startY <=y then
        if self.endX >= x and self.endY >= y then
            return true
        end
    end
    return false
end

function UIComponent:handleClick(x, y)
    local clickHandler = self.handlers[UIEventRegistry.CLICK]
    if clickHandler and self:isInside(x, y) then
        clickHandler(self, x, y)
    end
end

local UIContainer = UIComponent:new()
UIContainer.childrens = {}

function UIContainer:new(o)
    o = o or UIComponent:new(o)
    setmetatable(o, self)
    self.__index = self
    o.childrens = {}
    return o
end

function UIContainer:add(name, element)
    self.childrens[name] = element
end

function UIContainer:remove(name)
    self.childrens[name] = nil
end

function UIContainer:handleClick(x, y)
    UIComponent.handleClick(self, x, y)
    for _, children in pairs(self.childrens) do
        children:handleClick(x, y)
    end
end

local Display = {
    context = UIDrawContext:new({}, true),
    width = 0,
    height = 0,
    maxShift = 0,
    currentShift = 0,
    rootComponent = UIComponent:new()
}

function Display:new(monitor, rootComponent, o)
    expect.expect(1, monitor, "table")
    expect.expect(2, rootComponent, "table")
    expect.expect(3, o, "table", "nil")

    o = o or {}
    setmetatable(o, self)
    self.__index = self
    o.context = UIDrawContext:new(monitor, true)
    o.width, o.height = monitor.getSize()
    o.rootComponent = rootComponent
    return o
end

function Display:_drawBorder()
    self.context.monitor.setCursorPos(self.width, 1)
    self.context.monitor.write("\30")
    self.context.monitor.setCursorPos(self.width, self.height)
    self.context.monitor.write("\31")
    self.context.monitor.setCursorPos(self.width - 1, 1)
    if self.context.collapsed then
        self.context.monitor.write("\25")
    else
        self.context.monitor.write("\24")
    end
end

function Display:draw()
    self.context.monitor.clear()
    self.context.monitor.setCursorPos(1, 1 - self.currentShift)
    self.rootComponent:draw(self.context)
    local _, maxHeight = self.context.monitor.getCursorPos()
    self.maxShift = maxHeight - self.height + self.currentShift
    self:_drawBorder()
end

function Display:handleClick(x, y)
    if x == self.width then
        if y == 1 then
            self:scrollUp()
        elseif y == self.height then
            self:scrollDown()
        end
    elseif x == self.width - 1 and y == 1 then
        self:toggleCollapse()
    end
    self.rootComponent:handleClick(x, y)
end

function Display:scrollUp()
    if self.currentShift > 0 then
        self.currentShift = self.currentShift - 1
        self:draw()
    end
end

function Display:scrollDown()
    if self.currentShift < self.maxShift then
        self.currentShift = self.currentShift + 1
        self:draw()
    end
end

function Display:toggleCollapse()
    self.context.collapsed = not self.context.collapsed
    self:draw()
end

return {
    UIDrawContext = UIDrawContext,
    UIComponent = UIComponent,
    UIContainer = UIContainer,
    Display = Display,
}