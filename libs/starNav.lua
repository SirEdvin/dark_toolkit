local tinyMap = require("libs.tinyMap")
local aStar = require("libs.aStar")
local location = require("libs.location")
local core_utils = require("core.utils")


local SESSION_MAX_DISTANCE_DEFAULT = 32

local SESSION_COORD_CLEAR = 1
local SESSION_COORD_BLOCKED = 2
local SESSION_COORD_DISTANCE = math.huge
local MAIN_COORD_CLEAR = nil
local MAIN_COORD_BLOCKED = 1
local MAIN_COORD_DISTANCE = 1024


local function sup_norm(a, b)
    return math.max(math.abs(a.x - b.x), math.abs(a.y - b.y), math.abs(a.z - b.z))
end

local directions = {
    [vector.new(0, 0, 1)] = 0,
    [vector.new(-1, 0, 0)] = 1,
    [vector.new(0, 0, -1)] = 2,
    [vector.new(1, 0, 0)] = 3,
    [vector.new(0, 1, 0)] = 4,
    [vector.new(0, -1, 0)] = 5,
}

local function deltaToDirection(delta)
    local simplified_delta = core_utils.simplifyVector(delta)
    for vec, dir in pairs(directions) do
        if aStar.vectorEquals(simplified_delta, vec) then
            return dir
        end
    end
end

local function tryMove()
    for _ = 1, 4 do
        if turtle.forward() then
            return true
        end
        turtle.turnRight()
    end
    return false
end

local function blockIsTurtle(blockData)
    return blockData.name == "ComputerCraft:CC-TurtleAdvanced" or blockData.name == "ComputerCraft:CC-Turtle"
end


local Navigator = {
    position = nil, map = nil, sessionMap = nil, _distanceFunc = nil,
    sessionMaxDistance = 0, sessionMidPoint = nil
}

function Navigator:new(o, mapName)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    o.map = tinyMap.new(mapName)
    o._distanceFunc = function(a, b) return o:distanceFunc(a, b) end
    return o
end

function Navigator:updatePosition()
    local _move = turtle.up
    while not tryMove() do
        if not _move() then
            if _move == turtle.up then
                _move = turtle.down
                _move()
            else
                error("trapped in a ridiculous place")
            end
        end
    end

    local p1 = {gps.locate()}
    if #p1 == 3 then
        p1 = vector.new(unpack(p1))
    else
        error("no gps signal - phase 1")
    end

    if not turtle.back() then
        error("couldn't move to determine direction")
    end

    local p2 = {gps.locate()}
    if #p2 == 3 then
        p2 = vector.new(unpack(p2))
    else
        error("no gps signal - phase 2")
    end

    local direction = deltaToDirection(p1 - p2)
    if direction and direction < 4 then
        self.position = location.new(p2.x, p2.y, p2.z, direction)
    else
        error("cannot detect anything!")
    end
end

function Navigator:move(currPos, adjPos)
    local direction = deltaToDirection(adjPos - currPos)
    if direction then
        self.position:setHeading(direction)
        if direction == 4 then
            return self.position:up()
        elseif direction == 5 then
            return self.position:down()
        else
            return self.position:forward()
        end
    end
    return false
end

function Navigator:updateCoord(coord, isBlocked)
    if isBlocked then
        self.sessionMap:set(coord, SESSION_COORD_BLOCKED)
        self.map:set(coord, MAIN_COORD_BLOCKED)
    else
        self.sessionMap:set(coord, SESSION_COORD_CLEAR)
        self.map:set(coord, MAIN_COORD_CLEAR)
    end
end

function Navigator:detect(currPos, adjPos)
    local direction = deltaToDirection(adjPos - currPos)
    if direction then
        self.position:setHeading(direction)
        if direction == 4 then
            return turtle.detectUp()
        elseif direction == 5 then
            return turtle.detectDown()
        else
            return turtle.detect()
        end
    end
    return false
end

function Navigator:inspect(currPos, adjPos)
    local direction = deltaToDirection(adjPos - currPos)
    if direction then
        self.position:setHeading(direction)
        if direction == 4 then
            return turtle.inspectUp()
        elseif direction == 5 then
            return turtle.inspectDown()
        else
            return turtle.inspect()
        end
    end
    return false
end

function Navigator:scan(currPos)
    for _, pos in ipairs(aStar.adjacent(currPos)) do -- better order of checking directions
        self:updateCoord(pos, self:detect(currPos, pos))
    end
    self.map:save()
end

function Navigator:distanceFunc(a, b)
    local sessionMapA, sessionMapB = self.sessionMap:get(a), self.sessionMap:get(b)
    if sup_norm(a, self.sessionMidPoint) > self.sessionMaxDistance then
        return SESSION_COORD_DISTANCE -- first coord is outside the search region
    elseif sup_norm(b, self.sessionMidPoint) > self.sessionMaxDistance then
        return SESSION_COORD_DISTANCE -- second coord is outside the search region
    elseif sessionMapA == SESSION_COORD_BLOCKED or sessionMapB == SESSION_COORD_BLOCKED then
        return SESSION_COORD_DISTANCE -- one of the coords has been found to be blocked this during this session
    elseif sessionMapA == SESSION_COORD_CLEAR and sessionMapA == SESSION_COORD_CLEAR then
        return aStar.distance(a, b) -- both coords has been found to be clear this during this session
    elseif self.map:get(a) or self.map:get(b) then
        return MAIN_COORD_DISTANCE
    end
    return aStar.distance(a, b) -- we are assuming both coords are clear
end

function Navigator:moveTo(x, y, z, maxDistance)
    if turtle.getFuelLevel() == 0 then
        return false, "ran out of fuel"
    end
    if not self.position then
        self:updatePosition()
    end

    local goal = vector.new(tonumber(x), tonumber(y), tonumber(z))

    self.sessionMap = aStar.newMap()
    self.sessionMidPoint = vector.new(
        math.floor((goal.x + self.position.x)/2),
        math.floor((goal.y + self.position.y)/2),
        math.floor((goal.z + self.position.z)/2)
    )
    self.sessionMaxDistance = (
        (type(maxDistance) == "number" and maxDistance) or
        math.max(2*sup_norm(self.sessionMidPoint, goal), SESSION_MAX_DISTANCE_DEFAULT)
    )

    local path = aStar.compute(self._distanceFunc, self.position, goal)
    if not path then
        return false, "no known path to goal"
    end
    while not aStar.vectorEquals(self.position, goal) do
        local movePos = table.remove(path)
        while not self:move(self.position, movePos) do
            local blockPresent, blockData = self:inspect(self.position, movePos)
            local recalculate, isTurtle = false, false
             -- there is a turtle in the way
            if blockPresent and blockIsTurtle(blockData) then
                sleep(math.random(0, 3))
                local blockPresent2, blockData2 = self:inspect(self.position, movePos)
                 -- the turtle is still there
                if blockPresent2 and blockIsTurtle(blockData2) then
                    recalculate, isTurtle = true, true
                end
            elseif blockPresent then
                recalculate = true
            elseif turtle.getFuelLevel() == 0 then
                return false, "ran out of fuel"
            else
                sleep(1)
            end
            if recalculate then
                self:scan(self.position)
                if self.sessionMap:get(goal) == SESSION_COORD_BLOCKED then return false, "goal is blocked" end
                path = aStar.compute(self._distanceFunc, self.position, goal)
                if not path then
                    return false, "no known path to goal"
                end
                if isTurtle then
                    self.sessionMap:set(movePos, nil)
                end
                movePos = table.remove(path)
            end
        end
        if self.map:get(movePos) then
            self.map:set(movePos, MAIN_COORD_CLEAR)
        end
    end
    return true
end


return {
    Navigator = Navigator
}