--- Encodes a given string via base64.
-- Original one stolen from GitHub iskolbin/Ibase64 | MIT licence
--
-- @param b64 string The encoded base64 string.
-- @return string The decoded base64 string.
local function base64Decode(b64)
    local encoder = {}
	for b64code, char in pairs{[0]='A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','0','1','2','3','4','5','6','7','8','9','+','/','='} do
		encoder[b64code] = char:byte()
	end

	local decoder = {}
	for b64code, charcode in pairs(encoder) do
		decoder[charcode] = b64code
	end

	local pattern = '[^%w%+%/%=]'
	if decoder then
		local s62, s63
		for charcode, b64code in pairs(decoder) do
			if b64code == 62 then s62 = charcode
			elseif b64code == 63 then s63 = charcode
			end
		end
		pattern = ('[^%%w%%%s%%%s%%=]'):format(string.char(s62), string.char(s63))
	end

	b64 = b64:gsub(pattern, '')
	local cache = {}
	local t, k = {}, 1
	local n = #b64
	local padding = b64:sub(-2) == '==' and 2 or b64:sub(-1) == '=' and 1 or 0
	for i = 1, (padding > 0 and n-4 or n), 4 do
		local a, b, c, d = b64:byte(i, i+3)
		local s
		local v = decoder[a]*0x40000 + decoder[b]*0x1000 + decoder[c]*0x40 + decoder[d]
		s = string.char(bit32.extract(v,16,8), bit32.extract(v,8,8), bit32.extract(v,0,8))
		t[k] = s
		k = k + 1
	end

	if padding == 1 then
		local a, b, c = b64:byte(n-3, n-1)
		local v = decoder[a]*0x40000 + decoder[b]*0x1000 + decoder[c]*0x40
		t[k] = string.char(bit32.extract(v,16,8), bit32.extract(v,8,8))
	elseif padding == 2 then
		local a, b = b64:byte(n-3, n-2)
		local v = decoder[a]*0x40000 + decoder[b]*0x1000
		t[k] = string.char(bit32.extract(v,16,8))
	end
	return table.concat(t)
end

return {
    base64Decode = base64Decode
}