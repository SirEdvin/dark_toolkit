--- statue
-- This lib should help generate scultpures for statue workbench from png files
-- Partially copy-rewrite from https://github.com/1Turtle/sculpture/blob/main/sculpture.lua
--
-- @version 0.1
-- @author SirEdvin
-- @copyright 2023
-- @licence MIT (https://mit-license.org/)


-- Creates single color tint from pixel
local function getTint(skin, x, y)
    local pixel = skin:get_pixel(x,y)
    return 0xFF0000*pixel.r + 0xFF00*pixel.g + 0xFF*pixel.b
end



--- Creates a new shape for a statue workbench
local function newCube(tint, pos, size)
    return {
        tint = tint,
        x1 = pos.x-1,
        y1 = pos.y-1,
        z1 = pos.z-1,
        x2 = size and (pos.x+size.x-1) or pos.x,
        y2 = size and (pos.y+size.y-1) or pos.y,
        z2 = size and (pos.z+size.z-1) or pos.z
    }
end


--- Will generate a "textured surface" using optimized shapes.
-- The shapes will be optimized using a Greedy Mesh Algorithm.
local function generateTexturedSurface(skin, uv_x, uv_y, uv_width, uv_height)
    local shapes = {}
    local registered = {}

    local function register(begin_x,begin_y, width, height, tint)
        for y=begin_y, begin_y+height-1 do
            for x=begin_x, begin_x+width-1 do
                if not registered[x] then registered[x] = {} end
                registered[x][y] = tint
            end
        end
    end

    local function isRegistered(x,y)
        return (registered[x] and registered[x][y]) and true or false
    end

    -- Search for unused voxel
    local base_x, base_y = 1, uv_height
    for y=uv_y, uv_y+uv_height-1 do
        for x=uv_x, uv_x+uv_width-1 do
            -- Found one, get optimized size for shape
            if not isRegistered(x, y) then
                local tint = getTint(skin, x,y)
                local width, height = 1, 1

                -- Expand to width
                for sub_x=x+1, uv_x+uv_width-1 do
                    if getTint(skin, sub_x, y) == tint and not isRegistered(sub_x, y) then
                        width = width+1
                    else break end
                end

                -- Expand to height
                for sub_y=y+1, uv_y+uv_height-1 do
                    local validRow = true

                    -- Check row
                    for i=x, x+width-1 do
                        if getTint(skin, i, sub_y) ~= tint or isRegistered(i, sub_y) then
                            validRow = false
                            break
                        end
                    end

                    if validRow then
                        height = height+1
                    else break end
                end

                -- Register shape
                local shape = newCube(tint, vector.new(base_x,base_y-height+1,1), vector.new(width, height,1))
                table.insert(shapes, shape)
                register(x,y, width, height, tint)
            end

            base_x = base_x+1
        end
        base_y = base_y-1
        base_x = 1
    end

    return shapes
end

local function applySurface(cubes, x, y, z, rotation)
    local transformedCubes = {}
    for _,cube in ipairs(cubes) do
        local x1,y1,z1, x2,y2,z2 = cube.x1, cube.y1, cube.z1, cube.x2, cube.y2, cube.z2

        if rotation == 'x' then
            cube.x1 = x+x1-1
            cube.y1 = y+y1-1
            cube.z1 = z+z1-1

            cube.x2 = x+x2-1
            cube.y2 = y+y2-1
            cube.z2 = z+z2-1
        elseif rotation == 'z' then
            cube.z1 = z+x1-1
            cube.y1 = y+y1-1
            cube.x1 = x+z1-1

            cube.z2 = z+x2-1
            cube.y2 = y+y2-1
            cube.x2 = x+z2-1
        elseif rotation == 'y' then
            cube.x1 = x+x1-1
            cube.z1 = z+y1-1
            cube.y1 = y+z1-1

            cube.x2 = x+x2-1
            cube.z2 = z+y2-1
            cube.y2 = y+z2-1
        end

        table.insert(transformedCubes, cube)
    end
    return transformedCubes
end

local function insertAll(first, second)
    for _, el in ipairs(second) do
        table.insert(first, el)
    end
end

local function wrapPlayerHead(head)
    local modelCubes = {}
    -- Face
    local face = applySurface(generateTexturedSurface(head, 9, 9, 8, 8), 5, 1, 5, 'x')

    -- x=16 Side
    local x16_side = applySurface(generateTexturedSurface(head, 1, 9, 7, 8), 16-4, 1, 6, 'z')

    -- x=1 Side
    local x1_side = applySurface(generateTexturedSurface(head, 17, 9, 7, 8), 5, 1, 6, 'z')

    -- Back
    local back_side = applySurface(generateTexturedSurface(head, 25, 9, 6, 8), 6, 1, 16-4, 'x')

    -- Top
    local top_side = applySurface(generateTexturedSurface(head, 9, 1, 6, 6), 6, 8, 6, 'y')

    insertAll(modelCubes, face)
    insertAll(modelCubes, x16_side)
    insertAll(modelCubes, x1_side)
    insertAll(modelCubes, back_side)
    insertAll(modelCubes, top_side)
    return modelCubes
end


return {
    newCube = newCube,
    generateTexturedSurface = generateTexturedSurface,
    applySurface = applySurface,
    wrapPlayerHead = wrapPlayerHead
}