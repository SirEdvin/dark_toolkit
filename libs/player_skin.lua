--- player_skin
-- Library that allow you to extract player skin png image
-- Partially copy-rewrite from https://github.com/1Turtle/sculpture/blob/main/sculpture.lua
--
-- @version 0.1
-- @author SirEdvin
-- @copyright 2023
-- @licence MIT (https://mit-license.org/)

--- Downloads a given url while running a loading animation.
-- If completed, the loading bar will update its content to 'OK' or 'FAIL', depending on its result.
-- Will return a boolean, whenever it was successfull or not and then either the gotten content or the error message.

local pngImage = require("libs.png")
local base64 = require("libs.base64")

local function downloadFile(url, isBinary)
    if not http then
        return false, "The http API is not available but needed"
    end

    if type(isBinary) ~= "boolean" then isBinary  = false end

    -- Trying to request a download 
    if not http.request({ url = url, timeout = 7, binary = isBinary }) then
        return false, ("Could not request url \'%s\'"):format(url)
    end

    -- Waiting for download
    while true do
        local event, request_url, body = os.pullEvent()

        if event == "http_failure" and request_url == url then
            return false, ("Could not download url \'%s\': %s"):format(url, (body or "unknown"))
        elseif event == "http_success" and request_url == url then
            local content = body.readAll()
            body.close()

            return true, content
        end
    end
end


local function getSkinByName(value)
    --Validate given value
    local valid_username = (string.match(value, "^%a[%w_]*$") ~= nil and string.len(value) >= 3 and string.len(value) <= 16)
    local valid_UUID = (string.match(value, "^%x%x%x%x%x%x%x%x%-%x%x%x%x%-%x%x%x%x%-%x%x%x%x%-%x%x%x%x%x%x%x%x%x%x%x%x$") ~= nil or string.match(value, "^%x%x%x%x%x%x%x%x%%x%x%x%x%%x%x%x%x%%x%x%x%x%%x%x%x%x%x%x%x%x%x%x%x%x$") ~= nil)

    if not (valid_UUID or valid_username) then
        return false, "Given player name or UUID is invalid"
    end
    
    if not valid_UUID and valid_username then
        local profile_data_success, profile_data = downloadFile(("https://api.mojang.com/users/profiles/minecraft/%s"):format(value))

        -- Failed
        if not profile_data_success then
            return false, ("Error while requesting player data from Mojang: %s"):format(profile_data_success)
        end

        -- Decode profile data
        profile_data = textutils.unserialiseJSON(profile_data)
        if not (profile_data and profile_data.id) then
            return false, "Could not decode player profile data from Mojang"
        end

        -- Store UUID
        value = profile_data.id
    end

    local skin_url = ("https://sessionserver.mojang.com/session/minecraft/profile/%s?unsigned=false"):format(value)
    local profile_data_success, profile_data = downloadFile(skin_url)

    -- Failed
    if not profile_data_success then
        return false, "Could not decode player profile data from Mojang"
    end

    -- Decode profile data
    profile_data = textutils.unserializeJSON(profile_data)
    if not profile_data.properties then
        return false, "Could not parse profile data"
    end

    -- Process profile data
    for _, property in ipairs(profile_data.properties) do
        if property.name == "textures" then
            -- Decode texture data to get actual skin URL
            local texture_data = textutils.unserializeJSON(base64.base64Decode(property.value)) -- @todo Base 64 decode
            if not texture_data then
                return false, "Could not parse property of profile data"
            end

            if texture_data.textures and texture_data.textures.SKIN then
                local skin_data_success, skin_data = downloadFile(texture_data.textures.SKIN.url, true)

                -- Failed
                if not skin_data_success then
                    return false, ("Could not download player skin: %s"):format(skin_data)
                end

                local png_load_success, loadedPng = pcall(pngImage, nil, { input=skin_data })
                if not png_load_success then
                    return false, ("Could not load provided skin: %s"):format(loadedPng)
                end
                return true, loadedPng
            end
        end
    end
end

return {
    getSkinByName =getSkinByName
}