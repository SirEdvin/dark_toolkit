local DEFAULT_CHECK_FUEL_LEVEL = 10000

local function ensure_fuel_level(fuel_level)
    if fuel_level == nil then
        fuel_level = DEFAULT_CHECK_FUEL_LEVEL
    end
    local current_fuel_level = turtle.getFuelLevel()
    if current_fuel_level ~= "unlimited" then
        if (current_fuel_level <= fuel_level) then
            local creative_chest = peripheral.find("creative_chest")
            if creative_chest ~= nil then
                creative_chest.generate("minecraft:coal", 64)
                turtle.refuel(64)
            end
        end
    end
end

return {
    ensure_fuel_level = ensure_fuel_level
}