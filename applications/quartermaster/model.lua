local expect = require("cc.expect")

local shared_product = require("applications.shared.product")

local RedstoneLink = {name = "", side = "", mask = 1}

function RedstoneLink:new(name, side, mask, o)
    expect.expect(1, name, "string")
    expect.expect(2, side, "string")
    expect.expect(3, mask, "number")
    expect.expect(4, o, "table", "nil")

    o = o or {}
    setmetatable(o, self)
    self.__index = self
    o.name = name
    o.side = side
    o.mask = mask
    return o
end

local ProductionLine = {products = {}, name = "", redstoneLink = RedstoneLink:new("fake", "fake", 1), enabled = false}

function ProductionLine:new(name, products, redstoneLink, o)
    expect.expect(1, name, "string")
    expect.expect(2, products, "table")
    expect.expect(3, redstoneLink, "table")
    expect.expect(4, o, "table", "nil")

    o = o or {}
    setmetatable(o, self)
    self.__index = self
    o.name = name
    o.products = products
    o.redstoneLink = redstoneLink
    o.enabled = false
    return o
end

return {
    Product = shared_product.Product,
    ProductState = shared_product.ProductState,
    ProductRegistry = shared_product.ProductRegistry,
    RedstoneLink = RedstoneLink,
    ProductionLine = ProductionLine,
}