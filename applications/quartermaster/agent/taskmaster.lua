local doerlib = require("doerlib")
local bus = require("core.bus")

local quartermaster_model = require("applications.quartermaster.model")

local TaskmasterAgent = doerlib.BaseAgent:new()
TaskmasterAgent.productionLines = {}
TaskmasterAgent.productLinks = {}

function TaskmasterAgent:new(name, productionLines, o)
    o = o or doerlib.BaseAgent:new(name, 0, o)
    setmetatable(o, self)
    self.__index = self
    o.productionLines = productionLines
    o.productLinks = {}
    for _, productionLine in pairs(o.productionLines) do
        for _, product in pairs(productionLine.products) do
            o.productLinks[product] = productionLine
        end
    end
    return o
end

function TaskmasterAgent:configure()
    self._targetEvents = {
        bus.EventType.FABRICATION_INFORMATION
    }
    self._eventHandlers = {}
    self._eventHandlers[bus.EventType.FABRICATION_INFORMATION] = function(information)
        self:_fabricationInformationHandler(information)
    end
end

function TaskmasterAgent:_fabricationInformationHandler(information)
    for _, productionLine in pairs(self.productionLines) do
        productionLine.enabled = false
    end
    for product, _ in pairs(information.required) do
        if self.productLinks[product] then
            self.productLinks[product].enabled = true
        end
    end
    local redstoneMessage = {}
    for _, productionLine in pairs(self.productionLines) do
        if not redstoneMessage[productionLine.redstoneLink.name]then
            redstoneMessage[productionLine.redstoneLink.name] = {}
        end
        if not redstoneMessage[productionLine.redstoneLink.name][productionLine.redstoneLink.side] then
            redstoneMessage[productionLine.redstoneLink.name][productionLine.redstoneLink.side] = 0
        end
        if productionLine.enabled then
            redstoneMessage[productionLine.redstoneLink.name][productionLine.redstoneLink.side] = bit.bor(
                redstoneMessage[productionLine.redstoneLink.name][productionLine.redstoneLink.side],
                productionLine.redstoneLink.mask
            )
        end
    end
    for redstoneName, redstoneConfig in pairs(redstoneMessage) do
        bus.queueEvent(redstoneName, bus.EventType.REDSTONE_CONTROL_MESSAGE, redstoneConfig)
    end
end

function TaskmasterAgent:cleanup()
    doerlib.BaseAgent.cleanup(self)
    for _, productionLine in pairs(self.productionLines) do
        bus.queueEvent(productionLine.redstoneLink.name, bus.EventType.REDSTONE_CONTROL_MESSAGE, {
                left = 0, right = 0, top = 0, bottom = 0, front = 0, back = 0
        })
    end
end

return {
    TaskmasterAgent = TaskmasterAgent
}
