local doerlib = require("doerlib")
local bus = require("core.bus")
local logging = require("core.logging")

local _log = logging.create("applications.quartermaster.agent.sorter")

local _DEFAULT_SORTER_LOOP_DELAY = 1

local SorterAgent = doerlib.BaseAgent:new()
SorterAgent.chest = nil
SorterAgent.trash = nil
SorterAgent.interface = nil
SorterAgent.waitForUpdate = true
SorterAgent.excessiveProducts = {}


function SorterAgent:new(name, loop_delay, o)
    loop_delay = loop_delay or _DEFAULT_SORTER_LOOP_DELAY
    o = o or doerlib.BaseAgent:new(name, loop_delay, o)
    setmetatable(o, self)
    self.__index = self
    return o
end

function SorterAgent:configure()
    self.chest = peripheral.find("ironchest:diamond_chest")
    self.trash = peripheral.find("trashcans:item_trash_can_tile")
    self.interface = peripheral.find("refinedstorage:interface")
    if not self.chest or not self.trash or not self.interface then
        error("Cannot find required peripherals for setup to work!")
    end
    self._targetEvents = {bus.EventType.FABRICATION_INFORMATION}
    self._eventHandlers = {}
    self._eventHandlers[bus.EventType.FABRICATION_INFORMATION] = function(information)
        self:updateConfiguration(information)
    end
end

function SorterAgent:updateConfiguration(information)
    _log:info("Update sorter agent configuration ...")
    self.excessiveProducts = information.excessive
    self.waitForUpdate = false
end

function SorterAgent:perform()
    if not self.waitForUpdate then
        for slot, item in pairs(self.chest.list()) do
            if self.excessiveProducts[item.name] then
                if not pcall(self.chest.pushItems, peripheral.getName(self.trash), slot) then
                    _log:warn("Problem when push to trash can ...")
                end
            else
                if not pcall(self.chest.pushItems, peripheral.getName(self.interface), slot) then
                    _log:warn("Problem when push to interface ...")
                end
            end
        end
    end
end

return {
    SorterAgent = SorterAgent
}
