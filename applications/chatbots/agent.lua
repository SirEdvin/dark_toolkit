local doerlib = require("doerlib")

local _GUIDE_LIST = {
    oregen = (
        "In this pack several way to generate ores exists\n" ..
        "- Resourceful Bees: BEES\n" ..
        "- Botania: Orechid\n" ..
        "- Astral Sorcery: Mineralis Ritual\n" ..
        "- Nature's Aura: Powder of the Bountiful Core\n" ..
        "- Industrial Foregoing: Laser Drill\n" ..
        "- Occultism: Dimensional Mineshaft\n" ..
        "- Immersive Engineering: Excavator\n" ..
        "You always can find more information on Enigmatica 6 wiki"
    )
}


local ChatGuideAgent = doerlib.BaseAgent:new()
ChatGuideAgent.chatbox = nil

function ChatGuideAgent:new(name, chatbox, o)
    o = o or doerlib.BaseAgent:new(name, 0, o)
    setmetatable(o, self)
    self.__index = self
    o.name = name
    o.chatbox = chatbox
    return o
end

function ChatGuideAgent:configure()
    self._targetEvents = {"chat"}
    self._eventHandlers["chat"] = function(username, message)
        local startIndex, _ = string.find(message, "!guide ", 1)
        if startIndex == 1 then
            local targetGuide = string.gsub(message, "!guide ", "")
            if _GUIDE_LIST[targetGuide] then
                pcall(self.chatbox.sendMessage, _GUIDE_LIST[targetGuide])
            end
        end
    end
end

return {
    ChatGuideAgent = ChatGuideAgent,
}