local agent = require("applications.charcoal_energystation.agent")

return {
    BurnerAgent = agent.BurnerAgent,
    FuelAgent = agent.FuelAgent,
}