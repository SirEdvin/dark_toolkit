local agent = require("applications.supervisor.agent")
local ui = require("applications.supervisor.ui")

return {
    AgencySupervisorAgent = agent.AgencySupervisorAgent,
    SupervisorPanel = ui.SupervisorPanel,
}
