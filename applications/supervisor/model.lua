local expect = require("cc.expect")

local doerlib = require("doerlib")

local AgencyData = doerlib.AgencyInfomation:new("fale", "fake", "fake", "fake", {})

function AgencyData:new(o)
    expect.expect(1, o, "table")
    expect.field(o, "name", "string")
    expect.field(o, "state", "string")
    expect.field(o, "networkName", "string")
    expect.field(o, "type", "string")
    expect.field(o, "build", "string")
    expect.field(o, "version", "string")
    expect.field(o, "agents", "table")

    setmetatable(o, self)
    self.__index = self
    return o
end

function AgencyData:update(agencyInformation)
    self.name = agencyInformation.name
    self.build = agencyInformation.build
    self.version = agencyInformation.version
    self.agents = agencyInformation.agents
    self.state = agencyInformation.state
    self.networkName = agencyInformation.networkName
    self.type = agencyInformation.type
    return self
end

local AgencyList = {agencies = {}, agencyNames = {}}

local function agencyDataComp(data, aName, bName)
    local a = data[aName]
    local b = data[bName]
    -- type comparation
    local aindex = doerlib.AgencyType.toindex(a.type)
    local bindex = doerlib.AgencyType.toindex(b.type)
    if aindex ~= bindex then
        return aindex > bindex
    end
    -- name comparation
    return a.name < b.name
end

function AgencyList:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    o.agencies = {}
    o.agencyNames = {}
    return o
end

function AgencyList:updateData(agencyInformation)
    if self.agencies[agencyInformation.name] ~= nil then
        self.agencies[agencyInformation.name]:update(agencyInformation)
    else
        self.agencies[agencyInformation.name] = AgencyData:new(agencyInformation)
        table.insert(self.agencyNames, agencyInformation.name)
        table.sort(self.agencyNames, function(a, b) return agencyDataComp(self.agencies, a,  b) end)
    end
end

return {
    AgencyData = AgencyData,
    AgencyList = AgencyList,
}
