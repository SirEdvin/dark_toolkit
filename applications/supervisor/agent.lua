local doerlib = require("doerlib")
local bus = require("core.bus")

local supervisor_model = require("applications.supervisor.model")


local AgencySupervisorAgent = doerlib.BaseAgent:new()
AgencySupervisorAgent.data = supervisor_model.AgencyList:new()

function AgencySupervisorAgent:new(name, o)
    o = o or doerlib.BaseAgent:new(name, 0, o)
    setmetatable(o, self)
    self.__index = self
    o.data = supervisor_model.AgencyList:new()
    return o
end

function AgencySupervisorAgent:configure()
    self._targetEvents = {bus.EventType.AGENCY_INFORMATION_MESSAGE}
    self._eventHandlers = {}
    self._eventHandlers[bus.EventType.AGENCY_INFORMATION_MESSAGE] = function(data)
        self.data:updateData(data)
        bus.queueLocalEvent(bus.EventType.DATA_CHANGED, {source = self.name})
    end
end

return {
    AgencySupervisorAgent = AgencySupervisorAgent,
}
