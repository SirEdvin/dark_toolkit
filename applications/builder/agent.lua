local expect = require("cc.expect")

local doerlib = require("doerlib")
local logging = require("core.logging")

local builder_model = require("applications.builder.model")

local _log = logging.create("application.builder.agent")

local MaterialProviderAgent = doerlib.BaseAgent:new()
MaterialProviderAgent.rsPeripheral = nil
MaterialProviderAgent.dockPeripheralName = ""
MaterialProviderAgent.dockPeripheral = nil
MaterialProviderAgent.transferSide = ""
MaterialProviderAgent.registry = builder_model.ProductRegistry:new()


function MaterialProviderAgent:new(
        name, rsPeripheral, dockPeripheralName,
        transferSide, requiredProducts, loop_delay, o)
    expect.expect(1, name, "string")
    expect.expect(2, rsPeripheral, "table")
    expect.expect(3, dockPeripheralName, "string")
    expect.expect(4, transferSide, "string")
    expect.expect(5, requiredProducts, "table")
    expect.expect(6, loop_delay, "number", "nil")
    expect.expect(7, o, "table", "nil")

    o = o or doerlib.BaseAgent:new(name, loop_delay, o)
    setmetatable(o, self)
    self.__index = self
    o.name = name
    o.rsPeripheral = rsPeripheral
    o.dockPeripheralName = dockPeripheralName
    o.transferSide = transferSide
    o.registry = builder_model.ProductRegistry:new()
    for _, product in pairs(requiredProducts) do
        o.registry:add(product)
    end
    return o
end

function MaterialProviderAgent:perform()
    if self.dockPeripheral == nil then
        self.dockPeripheral = peripheral.find(self.dockPeripheralName)
        if self.dockPeripheral == nil then
            return
        end
    end
    if self.dockPeripheral.size() ~= nil and self.rsPeripheral.isConnected() then
        self.registry:update(self.dockPeripheral.list())
        local demand = self.registry:calculateDemand()
        if #demand == 0 then
            _log:success("Dock is full")
        end
        for product, requiredCount in pairs(demand) do
            local transferedCount = self.rsPeripheral.extractItem({name = product}, requiredCount, self.transferSide)
            if transferedCount == 0 then
                _log:warn("Cannot transfer", product, "because it not in stock ...")
            end
        end
    else
        _log:error("Seems, dock is empty or rs loading, waiting ...")
    end
end

function MaterialProviderAgent:configure()
end

return {
    MaterialProviderAgent = MaterialProviderAgent,
}