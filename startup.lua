package.path = package.path .. ";/dark_toolkit/?.lua;/dark_toolkit/?/init.lua"

local logging = require("core.logging")
local utils = require("core.utils")
local _log = logging.create("startup")
local version = require("_version")

print("Tweaked with DarkToolkit v" .. version.OS_VERSION, "build", version.OS_BUILD)

local label = os.getComputerLabel()
local serverFolder = settings.get("dark_toolkit.server_folder")

if label then

    label = string.gsub(label, "_.+", "")

    if fs.exists("/dark_toolkit/stored/" .. serverFolder .. "/" .. label .. ".lua") then
        local storedConfiguration = require("/dark_toolkit/stored/".. serverFolder .. "/" .. label)
        if storedConfiguration then
            _log:info("Starting stored configuration for " .. label)
            xpcall(storedConfiguration.start, utils.writeError)
        else
            _log:error("Problem with import stored configuration")
        end
    end

end

for _, filename in pairs(fs.list("/dark_toolkit/programs")) do
    local programName = string.gsub(filename, ".lua", "")
    shell.setAlias(programName, fs.combine("/dark_toolkit/programs", filename))
end
