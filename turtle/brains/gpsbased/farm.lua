local logger = require("core.logging")
local base = require("turtle.brains.gpsbased.base")

_log = logger.create("brain.farm")

local HoverFarmTurtle = base.AreaCycleTurtle:new("", 0, 0, 0, 0, 0, 0)
HoverFarmTurtle.resultChest = ""
HoverFarmTurtle.resultItems = {}

function HoverFarmTurtle:new(fuelChest, resultChest, resultItems, startX, startZ, startY, maxX, maxZ, maxY, o)
    o = o or base.AreaCycleTurtle:new(fuelChest, startX, startZ, startY, maxX, maxZ, maxY, o)
    setmetatable(o, self)
    self.__index = self
    o.resultChest = resultChest
    o.resultItems = resultItems
    return o
end

function HoverFarmTurtle:cellOperation()
    if turtle.detectDown() then
        turtle.digDown()
    end
    return true
end

function HoverFarmTurtle:dockOperation()
    local chestFind, chestErr = self:findChest(self.resultChest)
    if not chestFind then
        return false, chestErr
    end
    for slot = 1, 16 do
        local item = turtle.getItemDetail(slot)
        if item then
            if self.resultItems[item.name] then
                turtle.select(slot)
                local dropResult, dropErr = turtle.drop()
                if not dropResult then
                    return false, dropErr
                end
            end
        end
    end
    turtle.select(1)
    return true
end


local CreativeHoverFarmTurtle = base.CreativeAreaCycleTurtle:new(0, 0, 0, 0, 0, 0)
CreativeHoverFarmTurtle.husbandry_core = {}
CreativeHoverFarmTurtle.resultChest = ""

function CreativeHoverFarmTurtle:new(resultChest, startX, startZ, startY, maxX, maxZ, maxY, o)
    o = o or base.CreativeAreaCycleTurtle:new(startX, startZ, startY, maxX, maxZ, maxY, o)
    setmetatable(o, self)
    self.__index = self
    o.resultChest = resultChest
    return o
end

function CreativeHoverFarmTurtle:restock()
    local boneMealLevel = turtle.getItemCount(1)
    if boneMealLevel == 0 then
        self.creative_chest.generate("minecraft:bone_meal", 32)
    end
end

function CreativeHoverFarmTurtle:configure()
    base.CreativeAreaCycleTurtle.configure(self)
    self.husbandry_core = peripheral.find("husbandryAutomata")
    self.husbandry_core.setFuelConsumptionRate(6)
    if self.husbandry_core ~= nil and self.creative_chest ~= nil then
        _log:info("Creative hover farm turtle configured successfully")
    else
        _log:warn("There is a problem with creative hover farm configuration")
    end
end

function CreativeHoverFarmTurtle:cellOperation()
    local info = self.husbandry_core.look("block", "down")
    if info.age == info.maxAge then
        self.husbandry_core.harvest("down")
    else
        self:restock()
        local prev = turtle.getSelectedSlot()
        turtle.select(1)
        self.husbandry_core.use("block", "down")
        turtle.select(prev)
    end
    return true
end

function CreativeHoverFarmTurtle:dockOperation()
    local chestFind, chestErr = self:findChest(self.resultChest)
    if not chestFind then
        return false, chestErr
    end
    for slot = 2, 16 do
        local item = turtle.getItemDetail(slot)
        if item then
            turtle.select(slot)
            local dropResult, dropErr = turtle.drop()
            if not dropResult then
                return false, dropErr
            end
        end
    end
    turtle.select(2)
    return true
end

return {
    HoverFarmTurtle = HoverFarmTurtle,
    CreativeHoverFarmTurtle = CreativeHoverFarmTurtle,
}
