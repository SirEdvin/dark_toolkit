local expect = require("cc.expect")

local starNav = require("libs.starNav")
local logging = require("core.logging")

local _log = logging.create("turtle.base")

local TURTLE_FUEL_SLOT = 16

local function compareVectors(v1, v2)
    return v1.x == v2.x and v1.y == v2.y and v1.z == v2.z
end

local AreaBoundTurtle = {
    leftBound = vector.new(), rightBound = vector.new(),
    relativeBound = vector.new(), cellCount = 25, enabled = true,
}

function AreaBoundTurtle:new(startX, startZ, startY, maxX, maxZ, maxY, o)
    expect.expect(1, startX, "number")
    expect.expect(2, startY, "number")
    expect.expect(3, startZ, "number")
    expect.expect(4, maxX, "number")
    expect.expect(5, maxZ, "number")
    expect.expect(6, maxY, "number")
    expect.expect(7, o, "table", "nil")

    o = o or {}
    setmetatable(o, self)
    self.__index = self
    o.relativeBound = vector.new(maxX or 1, maxY or 1, maxZ or 1)
    o.cellCount = o.relativeBound.x * o.relativeBound.y * o.relativeBound.z
    o.nav = starNav.Navigator:new(nil, "areaBoundTurtleMap")
    o.leftBound = vector.new(startX, startY, startZ)
    o.rightBound = o.leftBound:add(o.relativeBound)
    return o
end

function AreaBoundTurtle:configure()
    local fuelLevel = turtle.getFuelLevel()
    if fuelLevel == 0 then
        local refuelResult, refuelErr = self:refuel()
        if not refuelResult then
            error(refuelErr)
        end
    end
    local positionUpdated = false
    repeat
        local r, err = xpcall(function() self.nav:updatePosition() end, function (err) _log:error(err) end)
        if r then
            _log:info("Position successfuly updated!")
            positionUpdated = true
        else
            _log:error(err)
            sleep(5)
        end
    until positionUpdated or not self.enabled
end

function AreaBoundTurtle:perform()
    error("This method should be overrided")
end

function AreaBoundTurtle:refuel()
    error("This function should be overwitten!")
end

function AreaBoundTurtle:_moveTo(x, z, y)
    local resultPosition = self.leftBound:add(vector.new(x or 0, y or 0, z or 0))
    local rightBoundOverlap = (
        resultPosition.x >= self.rightBound.x or
        resultPosition.y >= self.rightBound.y or
        resultPosition.z >= self.rightBound.z
    )
    if rightBoundOverlap then
        return false, "Cannot move, touch right bound"
    end
    local leftBoundOverlap = (
        resultPosition.x < self.leftBound.x or
        resultPosition.y < self.leftBound.y or
        resultPosition.z < self.leftBound.z
    )
    if leftBoundOverlap then
        return false, "Cannot move, touch left bound"
    end
    return self.nav:moveTo(resultPosition.x, resultPosition.y, resultPosition.z)
end

function AreaBoundTurtle:moveTo(x, z, y)
    local result, err = self:_moveTo(x, z, y)
    if not result and err then
        _log:error(err)
    end
end

function AreaBoundTurtle:getRelativePosition()
    return self.nav.location:tovector():sub(self.leftBound)
end

function AreaBoundTurtle:moveToNextPoint()
    local relativePos = self:getRelativePosition()
    local isReverseDirection = relativePos.z % 2 == 1
    local xDelta = 1
    if isReverseDirection then
        xDelta = -1
    end
    local xMoveRes, _ = self:_moveTo(relativePos.x + xDelta, relativePos.y, relativePos.z)
    if xMoveRes then
        return xMoveRes
    end
    -- canot move in x direction, try to expand by z axis
    local zMoveRes, _ = self:_moveTo(relativePos.x, relativePos.z + 1, relativePos.y)
    if zMoveRes then
        return true
    end
    -- cannot move by x or z, try to move by y
    local yMoveRes, _ = self:_moveTo(relativePos.x, relativePos.z, relativePos.y - 1)
    if yMoveRes then
        return true
    end
    self:moveTo()
end

function AreaBoundTurtle:cleanup()
    self:moveTo()
end

local AreaCycleTurtle = AreaBoundTurtle:new(0, 0, 0, 0, 0, 0)
AreaCycleTurtle.fuelChest = ""

function AreaCycleTurtle:new(fuelChest, startX, startZ, startY, maxX, maxZ, maxY, o)
    expect.expect(1, fuelChest, "string")

    o = o or AreaBoundTurtle:new(startX, startZ, startY, maxX, maxZ, maxY, o)
    setmetatable(o, self)
    self.__index = self
    o.fuelChest = fuelChest
    return o
end

function AreaCycleTurtle:cellOperation()
    print("No operation, just watching ...")
    return true
end

function AreaCycleTurtle:dockOperation()
    print("No operation, just waiting ...")
    return true
end

function AreaCycleTurtle:findChest(chest)
    local chestFound = false
    for _ = 1, 4 do
        local present, blockData = turtle.inspect()
        if present and blockData.name == chest then
            chestFound = true
            break
        end
        self.nav.position:left()
    end
    if not chestFound then
        return false, "cannot find chest"
    end
    return true
end

function AreaCycleTurtle:refuel()
    local fuelCount = turtle.getItemCount(TURTLE_FUEL_SLOT)
    if fuelCount > 0 then
        local prevSlot = turtle.getSelectedSlot()
        turtle.select(TURTLE_FUEL_SLOT)
        local refuelResult, err = turtle.refuel()
        if not refuelResult then
            return false, err
        end
        turtle.select(prevSlot)
        return true
    end
    if not compareVectors(self.leftBound, self.nav.position) then
        return false, "cannot refuel not in start point!"
    end
    -- trying to find chest
    local chestFind, chestFindErr = self:findChest(self.fuelChest)
    if not chestFind then
        return false, chestFindErr
    end
    local prevSlot = turtle.getSelectedSlot()
    turtle.select(TURTLE_FUEL_SLOT)
    local suckResult, suckResultErr = turtle.suck()
    if not suckResult then
        return false, suckResultErr
    end
    local refuelResult, refuelResultErr = turtle.refuel()
    if not refuelResult then
        return false, refuelResultErr
    end
    turtle.select(prevSlot)
    return true
end

function AreaCycleTurtle:perform()
    self:moveTo()
    sleep(1)
    local dockOpR, dockOpErr = self:dockOperation()
    if not dockOpR then
        return false, dockOpErr
    end
    local fuelCount = turtle.getFuelLevel()
    _log:info("Current fuel level " .. fuelCount .. "/" .. turtle.getFuelLimit())
    if  fuelCount <= self.cellCount * 2 then
        local refuelResult, refuelErr = self:refuel()
        if not refuelResult then
            return false, refuelErr
        end
    end
    for y = 0, self.relativeBound.y - 1 do
        for z = 0, self.relativeBound.z -1 do
            for x = 0, self.relativeBound.x - 1 do
                local realX = x
                if z % 2 == 1 then
                    realX = (self.relativeBound.x - 1) - realX
                end
                self:moveTo(realX, z, y)
                local celOpR, celOpErr = self:cellOperation()
                if not celOpR then
                    return false, celOpErr
                end
            end
        end
    end
    return true
end


local CreativeAreaCycleTurtle = AreaBoundTurtle:new(0, 0, 0, 0, 0, 0)
CreativeAreaCycleTurtle.creative_chest = {}

function CreativeAreaCycleTurtle:new(startX, startZ, startY, maxX, maxZ, maxY, o)
    o = o or AreaBoundTurtle:new(startX, startZ, startY, maxX, maxZ, maxY, o)
    setmetatable(o, self)
    self.__index = self
    return o
end

function CreativeAreaCycleTurtle:configure()
    AreaBoundTurtle.configure(self)
    self.creative_chest = peripheral.find("creative_chest")
end

function CreativeAreaCycleTurtle:cellOperation()
    print("No operation, just watching ...")
    return true
end

function CreativeAreaCycleTurtle:dockOperation()
    print("No operation, just waiting ...")
    return true
end

function CreativeAreaCycleTurtle:findChest(chest)
    local chestFound = false
    for _ = 1, 4 do
        local present, blockData = turtle.inspect()
        if present and blockData.name == chest then
            chestFound = true
            break
        end
        self.nav.position:left()
    end
    if not chestFound then
        return false, "cannot find chest"
    end
    return true
end

function CreativeAreaCycleTurtle:refuel()
    local fuelCount = turtle.getItemCount(TURTLE_FUEL_SLOT)
    if fuelCount < 0 then
        self.creative_chest.generate("minecraft:coal", 3)
    end
    local prevSlot = turtle.getSelectedSlot()
    turtle.select(TURTLE_FUEL_SLOT)
    local refuelResult, err = turtle.refuel(3)
    if not refuelResult then
        return false, err
    end
    turtle.select(prevSlot)
    return true
end

function CreativeAreaCycleTurtle:perform()
    self:moveTo()
    sleep(1)
    local dockOpR, dockOpErr = self:dockOperation()
    if not dockOpR then
        return false, dockOpErr
    end
    local fuelCount = turtle.getFuelLevel()
    _log:info("Current fuel level " .. fuelCount .. "/" .. turtle.getFuelLimit())
    if fuelCount ~= "unlimited" and  fuelCount <= self.cellCount * 2 then
        local refuelResult, refuelErr = self:refuel()
        if not refuelResult then
            return false, refuelErr
        end
    end
    for y = 0, self.relativeBound.y - 1 do
        for z = 0, self.relativeBound.z -1 do
            for x = 0, self.relativeBound.x - 1 do
                local realX = x
                if z % 2 == 1 then
                    realX = (self.relativeBound.x - 1) - realX
                end
                self:moveTo(realX, z, y)
                local celOpR, celOpErr = self:cellOperation()
                if not celOpR then
                    return false, celOpErr
                end
            end
        end
    end
    return true
end

return {
    AreaBoundTurtle = AreaBoundTurtle,
    AreaCycleTurtle = AreaCycleTurtle,
    CreativeAreaCycleTurtle = CreativeAreaCycleTurtle,
}
