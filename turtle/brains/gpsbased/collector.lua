local logging = require("core.logging")
local turtle = require "doerlib.extras.turtle"

local gpsbased_base = require("turtle.brains.gpsbased.base")
local turtle_utils = require("turtle.brains.utils")

local _CHEST_SLOT = 16
local _log = logging.create("turtle.brains.gpsbased.collector")

local LavaCollectorTurtle = gpsbased_base.AreaBoundTurtle:new(0, 0, 0, 0, 0, 0)

function LavaCollectorTurtle:new(startX, startZ, startY, maxX, maxZ, maxY, o)
    o = o or gpsbased_base.AreaBoundTurtle:new(startX, startZ, startY, maxX, maxZ, maxY, o)
    setmetatable(o, self)
    self.__index = self
    return o
end

function LavaCollectorTurtle:withChest(func, ...)
    local prevSlot = turtle.getSelectedSlot()
    local chestData = turtle.getItemDetail(_CHEST_SLOT)
    if chestData.name ~= "enderstorage:ender_chest" then
        error("Cannot find ender chest!")
    end
    turtle.select(_CHEST_SLOT)
    turtle.placeUp()
    local res = func(...)
    if turtle.getItemCount(_CHEST_SLOT) ~= 0 then
        error("Internal error, chest slot is full!")
    end
    turtle.digUp()
    turtle.select(prevSlot)
    return res
end

function LavaCollectorTurtle:_refuelWithChest()
    local chestPeriph = peripheral.wrap("top")
    local refuelResult = false
    for slot, item in pairs(chestPeriph.list()) do
        if item.name == "minecraft:lava_bucket" then
            local targetSlot = turtle_utils.firstEmptySlot()
            chestPeriph.pushItems("down", slot)
            local res, err = turtle_utils.withSlot(slot, turtle.refuel)
            if not res then
                _log:error("For some reason cannot refuel myself with lava bucket?")
                return false, "Cannot refuel with lava bucket"
            else
                turtle_utils.withSlot(slot, turtle.dropUp)
                return true
            end
        end
    end
end

function LavaCollectorTurtle:refuel()
    -- find lava bucket inside turtle
    for slot = 1, 15 do
        local slotData = turtle.getItemDetail(slot)
        if slotData.name == "minecraft:lava_bucket" then
            local res, err = turtle_utils.withSlot(slot, turtle.refuel)
            if not res then
                _log:error("For some reason cannot refuel myself with lava bucket?")
                return false, "Cannot refuel with lava bucket"
            else
                return true
            end
        end
    end
    -- find lava bucket inside chest
    return self:withChest(self._refuelWithChest, self)
end

function LavaCollectorTurtle:firstEmptyBucketSlot()
    -- check if have empty buckets inside inventory
    for slot = 1, 15 do
        local slotData = turtle.getItemDetail(slot)
        if slotData.name == "minecraft:bucket" then
            return slot
        end
    end
    return -1
end

function LavaCollectorTurtle:extractEmptyBuckets()
    local chestPeriph = peripheral.wrap("top")
    if chestPeriph ~= nil then
        error("You should setup chest first!")
    end
    for slot, item in pairs(chestPeriph.list()) do
        if item.name == "minecraft:bucket" then
            chestPeriph.pushItems("down", slot)
        end
        -- so, here magic three number
        -- why? Because one slot is for chest
        -- and next one is for first lava bucket
        if self:emptySlotCount() < 3 then
            break
        end
    end
end

function LavaCollectorTurtle:emptySlotCount()
    local counter = 0
    for slot = 1, 15 do
        if turtle.getItemCount(slot) == 0 then
            counter = counter + 1
        end
    end
    return counter
end

function LavaCollectorTurtle:deployLavaToChest()
    local chestPeriph = peripheral.wrap("top")
    if chestPeriph ~= nil then
        error("You should setup chest first!")
    end
    for slot = 1, 15 do
        local detail = turtle.getItemDetail(slot)
        if detail.name == "minecraft:lava_bucket" then
            local dropResult, dropErr = turtle_utils.withSlot(slot, turtle.dropUp)
            if not dropResult then
                return false, dropErr
            end
        end
    end
    return true
end

function LavaCollectorTurtle:isLavaDown()
    local inspect, details = turtle.inspectDown()
    if inspect then
        return details.name == "minecraft:lava" and details.state.level == 0
    end
    return false
end

function LavaCollectorTurtle:perform()
    local fuelCount = turtle.getFuelLevel()
    _log:info("Current fuel level " .. fuelCount .. "/" .. turtle.getFuelLimit())
    if  fuelCount <= self.cellCount * 2 then
        local refuelResult, refuelErr = self:refuel()
        if not refuelResult then
            return false, refuelErr
        end
    end
    if self:emptySlotCount() == 0 then
        local dropResult, _ = self:withChest(self.deployLavaToChest, self)
        if not dropResult then
            _log:warn("Cannot cleanup inventory, wait")
            sleep(20)
        end
        return true
    end
    local firstEmptyBucketSlot = self:firstEmptyBucketSlot()
    if firstEmptyBucketSlot == -1 then
        self:withChest(self.extractEmptyBuckets, self)
        firstEmptyBucketSlot = self:firstEmptyBucketSlot()
        if firstEmptyBucketSlot == -1 then
            _log:warn("Cannot find empty buckets, wait")
            sleep(20)
            return true
        end
    end
    turtle.select(firstEmptyBucketSlot)
    if self:isLavaDown() then
        turtle.placeDown()
    else
        self:moveToNextPoint()
    end
    return true
end

return {
    LavaCollectorTurtle = LavaCollectorTurtle
}