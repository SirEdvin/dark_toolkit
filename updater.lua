--- sos is a library that contains functions pertaining to SkyOS itself

local expect = require("cc.expect").expect
local pretty = require("cc.pretty")
local _versionLoaded, _version = pcall(require, "_version")

local presets = {
    gitlab = {projectID = 26949582, branch = "master", path="/dark_toolkit", cleanup = true}
}

local allowedModules = {
    [""] = true,
    ["core"] = true,
}

local allowedFiles = {
    ["programs/dark.lua"] = true,
    ["programs/dturtle.lua"] = true,
    ["programs/ddebug.lua"] = true,
}

-- file functions
local function fwrite(file, contents)
    expect(1, file, "string")
    expect(2, contents, "string")
    local f = fs.open(file, "w")
    f.write(contents)
    f.close()
end

local function hread(url)
    expect(1, url, "string")
    local h, err = http.get(url)
    if not h then return nil, err end
    local contents = h.readAll()
    h.close()
    return contents, nil
end

local function hreadWithHeaders(url)
    expect(1, url, "string")
    local h, err = http.get(url)
    if not h then return nil, err end
    local contents = h.readAll()
    local headers = h.getResponseHeaders()
    h.close()
    return {body = contents, headers = headers}, nil
end

---@param t1 {}
---@param t2 {}
local function concatTables(t1, t2)
    for i=1,#t2 do
        t1[#t1+1] = t2[i]
    end
    return t1
end


local function processTree(tree, projectID, path, branch, filterFunction)
    for _, v in pairs(tree) do
        if v.type == "blob" and string.match(v.path, ".*%.lua$") and filterFunction(v.path) then
            local fixedPath = string.gsub(v.path, "/", "%%2F")
            local url = ("https://gitlab.com/api/v4/projects/" .. projectID ..
                            "/repository/files/" .. fixedPath .. "/raw?ref=" ..
                            branch)
            local fileWritePath = fs.combine(path, v.path)
            print("Write file", fileWritePath)
            local data, err = hread(url)
            if err then print(url, err) end
            fwrite(fileWritePath, data)
        end
    end
end

local function checkIfUpdateRequired(projectID, branch)
    expect(1, projectID, "number", "nil")
    expect(3, branch, "string", "nil")
    projectID = projectID or presets.gitlab.projectID
    branch = branch or presets.gitlab.branch

    local versionDataUrl = "https://gitlab.com/api/v4/projects/" .. projectID .. "/repository/branches/" .. branch

    local rawVersionData, versionDataErr = hread(versionDataUrl)

    if versionDataErr then
        error(versionDataErr)
    end

    local versionData = textutils.unserializeJSON(rawVersionData)

    if _versionLoaded and versionData.commit.short_id == _version.OS_BUILD then
        return false, versionData
    end

    return true, versionData
end

local function downloadFile(filePath, projectID, branch)
    expect(2, filePath, "string")
    expect(2, projectID, "number", "nil")
    expect(3, branch, "string", "nil")

    projectID = projectID or presets.gitlab.projectID
    branch = branch or presets.gitlab.branch

    filePath = filePath:gsub("/", "%%2F")

    local url = (
        "https://gitlab.com/api/v4/projects/" .. projectID ..
        "/repository/files/" .. filePath .. "/raw?ref=" ..
        branch
    )
    local rawData, dataErr = hread(url)
    if dataErr then
        return nil, dataErr
    end
    return rawData
end

local function downloadRepo(filterFunction, force, projectID, path, branch, cleanup)
    expect(1, filterFunction, "function", "nil")
    expect(2, force, "boolean", "nil")
    expect(3, projectID, "number", "nil")
    expect(4, path, "string", "nil")
    expect(5, branch, "string", "nil")
    expect(6, cleanup, "boolean", "nil")

    force = force or false
    projectID = projectID or presets.gitlab.projectID
    path = path or presets.gitlab.path
    branch = branch or presets.gitlab.branch
    cleanup = cleanup or presets.gitlab.cleanup

    filterFunction = filterFunction or function() return true end

    local backupPath = path .. ".backup"

    local updateRequired, versionData = checkIfUpdateRequired(projectID, branch)

    if not updateRequired and not force then
        print("Nothing to update")
        return
    end

    local filesData = {}

    local repoUrl = "https://gitlab.com/api/v4/projects/" .. projectID ..
                        "/repository/tree?recursive=yes&per_page=100&pagination=keyset&ref=" ..
                        branch

    while repoUrl ~= nil do
        local rawData, repoErr = hreadWithHeaders(repoUrl)
        if repoErr then
            error(repoErr)
        end
        local newData = textutils.unserializeJSON(rawData.body)
        filesData = concatTables(filesData, newData)
        repoUrl = string.gmatch(rawData.headers["Link"], "<([^,]+)>; rel=" .. '"' .. "next" .. '"', 0)()
    end
    if cleanup then
        fs.move(path, backupPath)
    end
    local err_callback = function(_err) print("ERROR" .. _err) end
    if not xpcall(function() processTree(filesData, projectID, path, branch, filterFunction) end, err_callback) then
        fs.delete(path)
        fs.move(backupPath, path)
    else
        fs.delete(backupPath)
    end
    local formattedVersionData = {
        commit_id = versionData.commit.short_id,
        created_at = versionData.commit.created_at
    }
    fwrite(fs.combine(path, "__version__.lua"), textutils.serialise(formattedVersionData))
end

local function updateAllowance(file_content)
    local dependencyStarted = false
    for line in string.gmatch(file_content, "([^\n]+)") do
        if dependencyStarted then
            if line:sub(1, 3) == "-- " then
                local parsedDep = line:sub(4):gsub("%.", "/")
                if parsedDep:find(":") ~= nil then
                    allowedFiles[parsedDep:gsub(":", "/") .. ".lua"] = true
                else
                    allowedModules[parsedDep] = true
                end
            else
                break
            end
        end
        if line == "-- dependency" then
            dependencyStarted = true
        end
    end
end

local function updateDarkToolkit(reboot, force)
    expect(1, reboot, "boolean", "nil")
    expect(2, force, "boolean", "nil")
    reboot = reboot or false
    force = force or false
    -- build filter for darkToolkig update
    local serverFolder = settings.get("dark_toolkit.server_folder")
    if serverFolder == nil then
        settings.set("dark_toolkit.server_folder", "devserver")
        settings.save()
        serverFolder = "devserver"
    end
    local label = os.getComputerLabel()
    local storedPath = nil
    if label then
        label = string.gsub(label, "_.+", "")
        storedPath = "stored/" .. serverFolder .. "/" .. label .. ".lua"
        local stored_data = downloadFile(storedPath)
        if stored_data ~= nil then
            print("Processing stored file " .. storedPath)
            updateAllowance(stored_data)
        else
            print("Cannot find stored file " .. storedPath)
        end
    end

    local extra_programs = settings.get("dark_toolkit.extra_programs")

    if extra_programs ~= nil then
        for extra_program in string.gmatch(extra_programs, "[^,]+") do
            if extra_program ~= nil and extra_program ~= "" then
                local extra_program_path = "programs/" .. extra_program .. ".lua"
                local extra_program_data = downloadFile(extra_program_path)
                if extra_program_data ~= nil then
                    allowedFiles[extra_program_path] = true
                    updateAllowance(extra_program_data)
                else
                    print("Cannot find program " .. extra_program)
                end
            end
        end
    end

    print("Allowed modules for load")
    pretty.print(pretty.pretty(allowedModules))
    local filterFunction = function(path)
        -- print("Processing file " .. path)
        if path == storedPath then
            -- print("Save as stored")
            return true
        end
        if allowedFiles[path] then
            -- print("Save as allowedFiles")
            return true
        end
        if allowedModules[fs.getDir(path)] then
            -- print("Save as allowed Module")
            return true
        end
        return false
    end
    downloadRepo(filterFunction, force)
    print("Copy startup script to root")
    if not fs.exists("/startup") then
        fs.makeDir("/startup")
    end
    fs.delete("/startup/10_darktoolkit.lua")
    fs.copy(fs.combine(presets.gitlab.path, "startup.lua"), "/startup/10_darktoolkit.lua")
    if reboot then os.reboot() end
end

local function toolkitUpdateRequired()
    return checkIfUpdateRequired()
end

return {
    downloadRepo = downloadRepo, updateDarkToolkit = updateDarkToolkit,
    checkIfUpdateRequired = checkIfUpdateRequired, toolkitUpdateRequired = toolkitUpdateRequired
}