package.path = package.path .. ";/dark_toolkit/?.lua;/dark_toolkit/?/init.lua"

local selectedAction = select(1, ...)
local passed = { select(2, ...) }

local subcommands = {
    minefloor = {
        usage = "dturtle minefloor [w] [h]",
		flags = {
		}
    }
}

local action = subcommands[selectedAction]

if not action then
	for _, v in pairs(subcommands) do
		print(v.usage)
		print(" ")
	end
	return
end

-- get flag arguments
local function findFlag(arg)
	if arg.sub(1,1) == '-' then
		if not action.flags[arg] then
			return printError(('Unknown flag argument %s'):format(arg))
		end
	end
	return action.flags[arg]
end

local args = {}
local flags = {}
local lastFlagged = 0
for k, arg in pairs(passed) do
	if k ~= lastFlagged then
		local flagName = findFlag(arg)
		if flagName then
			flags[flagName] = passed[k + 1] or true
			lastFlagged = k + 1 or true
		else
			table.insert(args, arg)
		end
	end
end

if action == subcommands.minefloor then
    for curH = 1, args[2] do
        for curW = 1, args[1] do
            if turtle.detectDown() then
                turtle.digDown()
            end
            turtle.forward()
        end
        if turtle.detectDown() then
            turtle.digDown()
        end
        if curH % 2 == 1 then
            turtle.turnRight()
            turtle.forward()
            turtle.turnRight()
        else
            turtle.turnLeft()
            turtle.forward()
            turtle.turnLeft()
        end
    end
end
