package.path = package.path .. ";/dark_toolkit/?.lua;/dark_toolkit/?/init.lua"

local updater = require("updater")
local core_utils = require("core.utils")

local selectedAction = select(1, ...)
local passed = { select(2, ...) }

local subcommands = {
    update = {
        usage = "dark update [-r] [-f]",
		flags = {
			['-r'] = "restart",
			['-f'] = "force"
		}
    },
	install_tools = {
		usage = "dark install_tools"
	},
	save_point = {
		usage = "dark save_point",
	},
	debug = {
		usage = "dark debug [-n1] [-n2] [-n3]",
		flags = {
			['-n1'] = "first",
			['-n2'] = "second",
			['-n3'] = "third"
		}
	},
	run = {
		usage = "dark run [-u] [script] [script arg1] [script arg2]",
		flags = {
			['-u'] = "update"
		}
	}
}

local cute_debug_code = settings.get("dark_toolkit.cloud_code")

local action = subcommands[selectedAction]

if not action then
	for _, v in pairs(subcommands) do
		print(v.usage)
		print(" ")
	end
	return
end

-- get flag arguments
local function findFlag(arg)
	if arg.sub(1,1) == '-' then
		if not action.flags[arg] then
			return printError(('Unknown flag argument %s'):format(arg))
		end
	end
	return action.flags[arg]
end

local args = {}
local flags = {}
local lastFlagged = 0
for k, arg in pairs(passed) do
	if k ~= lastFlagged then
		local flagName = findFlag(arg)
		if flagName then
			flags[flagName] = passed[k + 1] or true
			lastFlagged = k + 1 or true
		else
			table.insert(args, arg)
		end
	end
end

if action == subcommands.update then
    updater.updateDarkToolkit(flags["restart"], flags["force"])
elseif action == subcommands.install_tools then
	shell.run("wget", "https://raw.githubusercontent.com/SquidDev-CC/mbs/master/mbs.lua", "mbs.lua")
	shell.run("wget", "https://cloud-catcher.squiddev.cc/cloud.lua")
	shell.run("mbs.lua", "install")
elseif action == subcommands.debug then
	local cc_code = settings.get("dark_toolkit.cloud_code")
	if cc_code == nil then
		print("Please, setup dark_toolkit.cloud_code")
		os.exit(0)
	end
	shell.run("cloud.lua", cc_code)
elseif action == subcommands.run then
	if flags["update"] then
		updater.updateDarkToolkit(false, false)
	end
	local script = require("scripts." .. args[1])
	local restArguments = {select(2, unpack(args))}
	if (restArguments == nil) then
		script.run()
	else
		script.run(unpack(restArguments))
	end
elseif subcommands.save_point then
	local locate_vector = core_utils.simplifyVector(vector.new(gps.locate()))
	settings.set("dark_toolkit.startX", locate_vector.x)
	settings.set("dark_toolkit.startY", locate_vector.y)
	settings.set("dark_toolkit.startZ", locate_vector.z)
	settings.save()
end