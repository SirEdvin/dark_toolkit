local function withTermColor(color, func, ...)
    local curColor = term.getTextColor()
    term.setTextColor(color)
    local funcResult = func(...)
    term.setTextColor(curColor)
    return funcResult
end

local function writeError(err)
    withTermColor(colors.red, term.write, err)
end

local function title(text)
    return string.upper(string.sub(text, 1, 1)) .. string.lower(string.sub(text, 2))
end

local function simplifyValue(val)
    local simplified_val = math.floor(val)
    if val == simplified_val then
        return simplified_val
    end
    if val < 0 then
        return simplified_val + 1
    end
    return simplified_val
end

local function simplifyVector(vec)
    return vector.new(simplifyValue(vec.x), simplifyValue(vec.y), simplifyValue(vec.z))
end

return {
    withTermColor = withTermColor,
    writeError = writeError,
    title = title,
    simplifyValue = simplifyValue,
    simplifyVector = simplifyVector,
}
