-- dependency
-- doerlib
-- applications.charcoal_energystation

local doerlib = require("doerlib")
local charcoal_energystation = require("applications.charcoal_energystation")

local burnerAgent = charcoal_energystation.BurnerAgent:new("burner", "metalbarrels:iron_tile", "thermal:machine_furnace", "ironchest:iron_chest")
local fuelAgent = charcoal_energystation.FuelAgent:new("fuel", "minecraft:hopper",  "ironchest:iron_chest", "powah:energy_cell")
local curAgency = doerlib.Agency:new(doerlib.AgencyType.CONTROLLER)
curAgency:configure()
curAgency:registerAgent(burnerAgent)
curAgency:registerAgent(fuelAgent)


return {start = function() curAgency:run() end}
