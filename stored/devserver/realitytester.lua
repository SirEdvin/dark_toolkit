local pattern = "^minecraft:"

local ignorewords = {
    "slab",
    "stair",
    "log",
    "plank",
    "ore",
    "wool",
    "candle",
    "coral",
    "wall",
    "fence",
    "glass",
}

local function start()
    local forger = peripheral.wrap("left")
    local registry = peripheral.wrap("right")
    local display = peripheral.wrap("top")
    local blocks = registry.list("block")
    for _, blockID in ipairs(blocks) do
        if blockID:find(pattern) ~= nil then
            local ignoreblock = false
            for _, ignore_pattern in ipairs(ignorewords) do
                if blockID:find(ignore_pattern) ~= nil then
                    ignoreblock = true
                end
            end
            if not ignoreblock then
                if pcall(forger.forgeReality, {block = blockID}) then
                    display.setItem(blockID)
                    print(blockID)
                    sleep(3)
                else
                    print("Block " .. blockID .. "in blocklist")
                end 
            end
        end
    end
end

return {
    start = start
}