local pretty = require "cc.pretty"

local function start()
    local p = peripheral.find("enderwireNetworkConnector")
    local networkData = p.inspectNetwork()
    local emitters = {}
    for _, data in pairs(networkData.elements) do
        if (data.deviceType == "lightEmitter") then
            table.insert(emitters, data)
        end
    end
    print("Count of lamp detected", #emitters)
    while true do
        local event, event_data = os.pullEvent("enderwire_computer_event")
        if (event_data.event == "attached" and event_data.deviceType == "lightEmitter") then
            table.insert(emitters, {name = event_data.name})
        elseif (event_data.event == "detached" and event_data.deviceType == "lightEmitter") then
            local recordIndex = -1
            for index, light in pairs(emitters) do
                if (light.name == event_data.name) then
                    recordIndex = index
                end
            end
            if recordIndex ~= -1 then
                table.remove(emitters, recordIndex)
            end
        elseif (event_data.event == "lever_enabled") then
            print("Turn lamps on")
            local termConfig = {}
            for _, light in pairs(emitters) do
                termConfig[light.name] = {lightLevel = 10 + math.random(10)}
            end
            p.configureElements(termConfig)
        elseif (event_data.event == "lever_disabled") then
            print("Turn lamps off")
            local termConfig = {}
            for _, light in pairs(emitters) do
                termConfig[light.name] = {lightLevel = 0}
            end
            p.configureElements(termConfig)
        elseif (event_data.event == "button_enabled") then
            print("color swapping")
            local termConfig = {}
            for _, light in pairs(emitters) do
                termConfig[light.name] = {color = {red = math.random(255), green = math.random(255), blue = math.random(255)}}
            end
            p.configureElements(termConfig)
        end
    end
end

return {
    start = start
}