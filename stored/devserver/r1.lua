local pretty = require "cc.pretty"

local function start()
    local networkConnector = peripheral.wrap("left")
    local networkElements = networkConnector.inspectNetwork().elements
    local remoteMonitor = peripheral.find("monitor")

    local function extractElement(uuid)
        for _, element in pairs(networkElements) do
            if element.UUID == uuid then
                return element
            end
        end
        return nil
    end

    local function filterElements(filter_func)
        local filteredElements = {}
        for _, element in pairs(networkElements) do
            if filter_func(element) then
                table.insert(filteredElements, element.UUID)
            end
        end
        return filteredElements
    end

    local function buildUpdateTable(lamps, eventData)
        local update_data = {}
        for _, lamp in pairs(lamps) do
            if (eventData.event == "lever_enabled") then
               update_data[lamp] = {color = {red = math.random(255), green = math.random(255), blue = math.random(255)}, enabled = true}
            else
                update_data[lamp] = {enabled = false}
            end
        end
        return update_data
    end

    while true do
        local _, eventData = os.pullEvent("enderwire_computer_event")
        local leverElementData = extractElement(eventData.element)
        if (leverElementData ~= nil) then
            if (leverElementData.position.z == 1) then
                local update_data = buildUpdateTable(filterElements(function(element)
                    return element.deviceType == "light_emitter" and element.position.z == 4
                end), eventData)
                networkConnector.configureElements(update_data)
            elseif (leverElementData.position.z ==-2) then
                local update_data = buildUpdateTable(filterElements(function(element)
                    return element.deviceType == "light_emitter" and element.position.z == 6
                end), eventData)
                networkConnector.configureElements(update_data)
            elseif (leverElementData.position.z == 3) then
                if (eventData.event == "lever_enabled") then
                    sleep(0.5)
                    remoteMonitor.setCursorPos(1, 1)
                    remoteMonitor.write("Hello, world!")
                else
                    remoteMonitor.clear()
                end
            end
        end
    end
end

return {
    start = start
}