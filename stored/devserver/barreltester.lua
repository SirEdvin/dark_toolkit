local utils = require("core.utils")

local b1_name = "minecraft:barrel_0"
local b2_name = "minecraft:barrel_1"
local b3_name = "minecraft:barrel_2"

local function moveItems()
    local b1 = peripheral.wrap(b1_name)
    local b2 = peripheral.wrap(b2_name)
    local b3 = peripheral.wrap(b3_name)
    while true do
        b1.pushItems(b2_name, 1)
        b2.pushItems(b3_name, 1)
        b3.pushItems(b1_name, 1)
        sleep(0.1)
    end
end

local function eventPrinter()
    while true do
        local event, something = os.pullEvent()
        if event == "peripheral" then
            print("Peripheral attached " .. something)
        elseif event == "peripheral_detach" then
            print("Peripheral detached " .. something)
        end
    end
end 

local function start()
    parallel.waitForAll(moveItems, eventPrinter)
end

return {
    start = start
}