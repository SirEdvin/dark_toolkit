local function randomColor()
    return {red = math.random(255), green = math.random(255), blue = math.random(255)}
end

local function start()
    local p = peripheral.find("statueWorkbench")
    while true do
        p.setCubes({{x1=0, x2=8, y1=0, y2=8, z1=0, z2=8, color = randomColor()}})
        sleep(2.5)
        p.setCubes({{x1=8, x2=16, y1=8, y2=16, z1=8, z2=16, color = randomColor()}})
        sleep(2.5)
        p.setCubes({{x1=0, x2=16, y1=0, y2=48, z1=0, z2=16, color = randomColor()}})
        sleep(2.5)
        p.setCubes({{x1=0, x2=32, y1=0, y2=32, z1=0, z2=32, color = randomColor()}})
        sleep(2.5)
    end
end

return {
    start = start
}