-- imports
local doerlib = require("doerlib")
local doerlib_extras = require("doerlib.extras")

local redstoneAgent = doerlib_extras.RedstoneAgent:new()

local curAgency = doerlib.Agency:new(doerlib.AgencyType.SLAVE)
curAgency:configure()
curAgency:registerAgent(redstoneAgent)

return {
    start = function() curAgency:run() end
}
