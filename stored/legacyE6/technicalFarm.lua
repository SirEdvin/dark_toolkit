-- imports
local doerlib = require("doerlib")
local quartermaster = require("applications.quartermaster")

local farmAgent = quartermaster.TaskmasterAgent:new("Technical Farm", {
    quartermaster.ProductionLine:new(
        "wheat",
        {"minecraft:wheat_seeds", "minecraft:wheat"},
        quartermaster.RedstoneLink:new("redstoneSlave_TF1", "back", 1)
    ),
    quartermaster.ProductionLine:new(
        "darkOak",
        {
            "minecraft:dark_oak_log", "minecraft:dark_oak_leaves",
            "minecraft:dark_oak_sapling", "minecraft:apple"
        },
        quartermaster.RedstoneLink:new("redstoneSlave_TF1", "back", 2)
    ),
    quartermaster.ProductionLine:new(
        "oak",
        {
            "minecraft:oak_log",
            "minecraft:oak_leaves",
            "minecraft:oak_sapling",
            "minecraft:apple"
        },
        quartermaster.RedstoneLink:new("redstoneSlave_TF1", "left", 1)
    ),
    quartermaster.ProductionLine:new(
        "sugarCane",
        {
            "minecraft:sugar_cane"
        },
        quartermaster.RedstoneLink:new("redstoneSlave_TF1", "left", 4)
    ),
    quartermaster.ProductionLine:new(
        "grass",
        {
            "minecraft:grass"
        },
        quartermaster.RedstoneLink:new("redstoneSlave_TF1", "left", 2)
    ),
    quartermaster.ProductionLine:new(
        "spruce",
        {
            "minecraft:spruce_log",
            "minecraft:spruce_leaves",
            "minecraft:spruce_sapling"
        },
        quartermaster.RedstoneLink:new("redstoneSlave_TF1", "front", 1)
    ),
    quartermaster.ProductionLine:new(
        "netherWart",
        {
            "minecraft:nether_wart"
        },
        quartermaster.RedstoneLink:new("redstoneSlave_TF1", "front", 2)
    ),
    quartermaster.ProductionLine:new(
        "carrot",
        {
            "minecraft:carrot"
        },
        quartermaster.RedstoneLink:new("redstoneSlave_TF1", "front", 4)
    ),
    quartermaster.ProductionLine:new(
        "potato",
        {
            "minecraft:potato",
            "minecraft:poisonous_potato",
        },
        quartermaster.RedstoneLink:new("redstoneSlave_TF1", "front", 8)
    ),
    quartermaster.ProductionLine:new(
        "onion",
        {
            "farmersdelight:onion"
        },
        quartermaster.RedstoneLink:new("redstoneSlave_TF1", "left", 8)
    ),
    quartermaster.ProductionLine:new(
        "flax",
        {
            "supplementaries:flax",
            "supplementaries:flax_seeds",
        },
        quartermaster.RedstoneLink:new("redstoneSlave_TF1", "back", 8)
    ),
    quartermaster.ProductionLine:new(
        "tomato",
        {
            "farmersdelight:tomato",
            "farmersdelight:tomato_seeds"
        },
        quartermaster.RedstoneLink:new("redstoneSlave_TF1", "right", 1)
    ),
    quartermaster.ProductionLine:new(
        "sweetPotato",
        {
            "simplefarming:sweet_potato",
            "simplefarming:sweet_potato_seeds",
        },
        quartermaster.RedstoneLink:new("redstoneSlave_TF1", "right", 2)
    ),
    quartermaster.ProductionLine:new(
        "rice",
        {
            "farmersdelight:rice",
            "farmersdelight:rice_panicle",
            "farmersdelight:straw"
        },
        quartermaster.RedstoneLink:new("redstoneSlave_TF1", "right", 4)
    ),
    quartermaster.ProductionLine:new(
        "menril",
        {
            "integrateddynamics:menril_sapling",
            "integrateddynamics:menril_log",
            "integrateddynamics:menril_leaves",
            "integrateddynamics:menril_berries",
        },
        quartermaster.RedstoneLink:new("redstoneSlave_TF1", "right", 8)
    ),
    quartermaster.ProductionLine:new(
        "acacia",
        {
            "minecraft:acacia_log",
            "minecraft:acacia_leaves",
            "minecraft:acacia_sapling",
            "alexsmobs:acacia_blossom"
        },
        quartermaster.RedstoneLink:new("redstoneSlave_TF1", "back", 4)
    ),
    quartermaster.ProductionLine:new(
        "Crimson",
        {
            "minecraft:crimson_fungus",
            "minecraft:crimson_stem",
            "minecraft:nether_wart_block"
        },
        quartermaster.RedstoneLink:new("redstoneSlave_TF2", "left", 1)
    ),
    quartermaster.ProductionLine:new(
        "Mystical Pink flower",
        {
            "botania:pink_mystical_flower",
        },
        quartermaster.RedstoneLink:new("redstoneSlave_TF2", "left", 2)
    ),
})


local sorterAgent = quartermaster.SorterAgent:new("Technical sorter")

local curAgency = doerlib.Agency:new(doerlib.AgencyType.CONTROLLER)
curAgency:configure()
curAgency:registerAgent(sorterAgent)
curAgency:registerAgent(farmAgent)

return {start = function() curAgency:run() end}
