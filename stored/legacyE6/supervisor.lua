-- imports
local doerlib = require("doerlib")
local doerlib_extras = require("doerlib.extras")
local supervisorApp = require("applications.supervisor")
local bus = require("core.bus")

local supervisorAgent = supervisorApp.AgencySupervisorAgent:new("Supervisor")
local supervisorPanel = supervisorApp.SupervisorPanel:new(supervisorAgent)
local screenAgent = doerlib_extras.ScreenAgent:new("Screen")

screenAgent:addMonitor(peripheral.wrap("right"), supervisorPanel, supervisorAgent.name)
local curAgency = doerlib.Agency:new(doerlib.AgencyType.SUPERVISOR)

curAgency:configure({bus.EventType.AGENCY_INFORMATION_MESSAGE})
curAgency:registerAgent(supervisorAgent)
curAgency:registerAgent(screenAgent)

return {
    start = function() curAgency:run() end
}
