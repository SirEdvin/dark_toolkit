-- imports
local doerlib = require("doerlib")

local curAgency = doerlib.Agency:new(doerlib.AgencyType.CONTROLLER)
curAgency:configure()


return {start = function() curAgency:run() end}
