-- imports
local doerlib = require("doerlib")
local chatbots = require("applications.chatbots")

local chatbotAgent = chatbots.ChatGuideAgent:new("Guide Bot", peripheral.wrap("right"))

local curAgency = doerlib.Agency:new(doerlib.AgencyType.CONTROLLER)
curAgency:configure()
curAgency:registerAgent(chatbotAgent)


return {start = function() curAgency:run() end}
