-- imports
local doerlib = require("doerlib")
local quartermaster = require("applications.quartermaster")

local farmAgent = quartermaster.TaskmasterAgent:new("Stone Factory", {
    quartermaster.ProductionLine:new(
        "Granite Engraved",
        {"masonry:graniteengraved"},
        quartermaster.RedstoneLink:new("redstoneSlave_SF1", "left", 1)
    ),
    quartermaster.ProductionLine:new(
        "Black chiseled sandstone",
        {"byg:black_chiseled_sandstone"},
        quartermaster.RedstoneLink:new("redstoneSlave_SF1", "left", 2)
    ),
    quartermaster.ProductionLine:new(
        "Sand and Aquamarine shale",
        {"minecraft:sand", "astralsorcery:aquamarine_sand_ore"},
        quartermaster.RedstoneLink:new("redstoneSlave_SF1", "left", 4)
    ),
    quartermaster.ProductionLine:new(
        "Basalt large bricks",
        {"embellishcraft:basalt_large_bricks"},
        quartermaster.RedstoneLink:new("redstoneSlave_SF1", "left", 8)
    ),
})


local sorterAgent = quartermaster.SorterAgent:new("Stone Factory sorter")

local curAgency = doerlib.Agency:new(doerlib.AgencyType.CONTROLLER)
curAgency:configure()
curAgency:registerAgent(sorterAgent)
curAgency:registerAgent(farmAgent)


return {start = function() curAgency:run() end}
