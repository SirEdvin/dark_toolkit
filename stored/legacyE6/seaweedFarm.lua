
local agency = require("agency")
local agents = require("agents")
local brains = require("brains")

local curAgency = agency.Agency:new(agency.AgencyType.CONTROLLER)

local resultItems = {}
resultItems["sushigocrafting:seaweed"] = true

local turtleBrain = brains.HoverFarmTurtle:new(
    "enderstorage:ender_chest", "quark:nether_brick_chest", resultItems,
    1982, -4346, 21, 6, 6
)
local turtleAgent = agents.TurtleAgent:new(turtleBrain)

curAgency:registerAgent(turtleAgent)
curAgency:configure()

return {
    start = function() curAgency:run() end
}
