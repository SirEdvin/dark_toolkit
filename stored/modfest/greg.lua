local function check_fuel()
    if (turtle.getFuelLevel() <= 10000) then
        local creative_chest = peripheral.wrap("creative_chest_1")
        if creative_chest ~= nil then
            creative_chest.generate("minecraft:coal", 64)
            turtle.refuel(64)
        end
    end
end

local function getDistance(player)
    return math.sqrt(player.x * player.x + player.y * player.y + player.z * player.z)
end

local function closedMessage(player_name, chatter)
    if player_name == "SirEdvin" then
        chatter.setMessage("Hello, master!")
    else
        chatter.setMessage("Hello there, " .. player_name .. ". Come closer.")
    end
end

local function helpMaster(player_name, chatter)
    if player_name == "SirEdvin" then
        chatter.setMessage("Master, did you build something already?")
    else
        chatter.setMessage("My master have no idea what to build, help him!")
    end
end


local function start()
    local targeted_player = nil
    local last_distance = 100
    local scanner = peripheral.wrap("universal_scanner_1")
    local chatter = peripheral.wrap("left")
    while true do
        check_fuel()
        local all_players, reason = scanner.scan("player")
        if all_players ~= nil then
            if #all_players > 0 then
                if targeted_player == nil then
                    local random_player = all_players[math.random(#all_players)]
                    targeted_player = random_player.displayName
                    last_distance = getDistance(random_player)
                    closedMessage(targeted_player, chatter)
                else
                    local same_player = nil
                    for _, player in ipairs(all_players) do
                        if player.displayName == targeted_player then
                            same_player = player
                        end
                    end
                    if same_player == nil then
                        targeted_player = nil
                    else
                        local new_distance = getDistance(same_player)
                        if new_distance <= last_distance then
                            helpMaster(targeted_player, chatter)
                        else
                            closedMessage(targeted_player, chatter)
                        end
                        last_distance = new_distance
                    end
                end
            else
                chatter.setMessage("I am so lonely here!")
            end
        else
            print(reason)
        end
        sleep(2.2)
    end
end

return {
    start = start
}