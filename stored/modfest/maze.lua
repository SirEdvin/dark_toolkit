-- dependency
-- doerlib

local pretty = require("cc.pretty")
local expect = require("cc.expect")

local doerlib = require("doerlib")
local bus = require("core.bus")
local logging = require("core.logging")


local _log = logging.create("maze")

local MazeAgent = doerlib.BaseAgent:new()
MazeAgent.forger = {}
MazeAgent.clusters = {}
MazeAgent.hidden_door = {}
MazeAgent.opened_cluster_chance = 0.5
MazeAgent.mystical_cluster_chance = 0.01
MazeAgent.max_opened_clusters = 0
MazeAgent.disabled_maze_counter = 0
MazeAgent.expected_cluster_size = 6

function MazeAgent:new(name, loop_delay, o)
    expect.expect(1, name, "string")
    expect.expect(2, loop_delay, "number", "nil")
    expect.expect(3, o, "table", "nil")
    o = o or doerlib.BaseAgent:new(name, loop_delay, o)
    setmetatable(o, self)
    self.__index = self
    o.name = name
    return o
end


function MazeAgent:isClose(first_pos, second_pos)
    return math.abs(first_pos.x - second_pos.x) + math.abs(first_pos.y - second_pos.y) + math.abs(first_pos.z - second_pos.z) == 1
end

function MazeAgent:collect_clusters()
    local anchors = self.forger.detectAnchors()
    local hidden_door = {}
    local clusters = {}
    local posClusterMap = {}
    for _, pos in ipairs(anchors) do
        if pos.y > -10 then
            if pos.y > 0 then
                table.insert(hidden_door, pos)
            else
                local is_cluster_found = false
                for posCandidate, cluster_index in pairs(posClusterMap) do
                    if self:isClose(pos, posCandidate) then
                        table.insert(clusters[cluster_index], pos)
                        posClusterMap[pos] = cluster_index
                        is_cluster_found = true
                        break
                    end
                end
                if not is_cluster_found then
                    table.insert(clusters, {pos})
                    posClusterMap[pos] = #clusters
                end
            end
        end
    end
    return clusters, hidden_door
end

local cluster_state = {block = "minecraft:stone_bricks"}
local finish_maze_state = {block = "minecraft:black_glazed_terracotta"}
local open_options = {playerPassable=true, invisible = true}
local closed_options = {playerPassable=false, invisible = false}


local function insertAll(first, second)
    for _, el in ipairs(second) do
        table.insert(first, el)
    end
end


function MazeAgent:rebuild_maze()
    local open_poses = {}
    local closed_poses = {}
    -- sort clusters
    for _, cluster in ipairs(self.clusters) do
        if math.random() <= self.opened_cluster_chance and #open_poses < self.max_opened_clusters * self.expected_cluster_size then
            insertAll(open_poses, cluster)
        else
            insertAll(closed_poses, cluster)
        end
    end
    local batch = {
        {closed_poses, cluster_state, closed_options},
        {open_poses, cluster_state, open_options},
    }
    local result, apply_result, err = pcall(self.forger.batchForgeRealityPieces, batch)
    if not result then
        bus.queueEvent("controller", "logging", "Issue with maze building: " .. apply_result, true)
        return false, {0, 0, 0}
    end
    if not apply_result then
        bus.queueEvent("controller", "logging", "Issue with maze building: " .. err, true)
        return false, {0, 0, 0}
    end
    return apply_result, {#open_poses / self.expected_cluster_size, #closed_poses / self.expected_cluster_size, 0}
end

function MazeAgent:build_disabled_maze()
    -- TODO: add closed gate logic
    local blocks = {}
    local top_blocks = {}
    for _, cluster in ipairs(self.clusters) do
        for _, pos in ipairs(cluster) do
            if pos.y == -1 then
                table.insert(top_blocks, pos)
            else
                table.insert(blocks, pos)
            end
        end
    end
    self.forger.batchForgeRealityPieces({
        {blocks, cluster_state, open_options},
        {top_blocks, finish_maze_state, closed_options}
    })
end


function MazeAgent:build_closed_maze()
    local blocks = {}
    local closed_block = {}
    for _, cluster in ipairs(self.clusters) do
        for _, pos in ipairs(cluster) do
            if pos.x <= -3 and pos.z >= 2 then
                table.insert(closed_block, pos)
            else
                table.insert(blocks, pos)
            end
        end
    end
    self.forger.batchForgeRealityPieces({
        {blocks, cluster_state, open_options},
        {closed_block, finish_maze_state, closed_options}
    })
end

function MazeAgent:perform()
    if self.disabled_maze_counter > 0 then
        if self.disabled_maze_counter < 500 then
            self.disabled_maze_counter = self.disabled_maze_counter - 1
            if self.disabled_maze_counter <= 0 then
                bus.queueEvent("controller", "logging", "Maze will be rebuild at next round", true)
            else
                bus.queueEvent("controller", "logging", "Maze will be rebuild after " .. self.disabled_maze_counter .. " rounds", true)
            end
        end
    else
        local maze_rebuilt = false
        local stats = {0, 0, 0}
        while not maze_rebuilt do
            maze_rebuilt, stats = self:rebuild_maze()
            if not maze_rebuilt then
                bus.queueEvent("controller", "logging", "Maze rebuilt failed", true)
            else
                bus.queueEvent("controller", "logging", "Maze rebuilt: open doors: " .. stats[1] .. ", closed doors: " .. stats[2] .. ", mystical doors: " .. stats[3], true)
            end
        end
    end
end

function MazeAgent:_handleBlockChange(new_state)
    if new_state.name == "minecraft:stone_button" then
        if new_state.properties.powered == "true" and self.disabled_maze_counter < 1 then
            self.disabled_maze_counter = 6
            self:build_disabled_maze()
            bus.queueEvent("controller", "logging", "Maze was finished! Maze open now", true)
        end
    else
        if new_state.properties.powered == "true" then
            self.disabled_maze_counter = 1000
            self:build_closed_maze()
            bus.queueEvent("controller", "logging", "Maze was closed", true)
        else
            self.disabled_maze_counter = 0
            self:rebuild_maze()
            bus.queueEvent("controller", "logging", "Maze is reopened again!", true)
        end
    end
end

function MazeAgent:configureHiddenDoor()
    self.forger.forgeRealityPieces(self.hidden_door, cluster_state, {playerPassable=true})
end

function MazeAgent:configure()
    _log:info("Start configuration")
    self.forger = peripheral.find("reality_forger")
    self.clusters, self.hidden_door = self:collect_clusters()
    _log:info("Clusters collected")
    self.max_opened_clusters = #self.clusters / 2 + 1
    self._targetEvents = {"block_change"}
    self._eventHandlers["block_change"] = function(block_pos, old_state, new_state) self:_handleBlockChange(new_state) end
    self:configureHiddenDoor()
end


local mazeAgent = MazeAgent:new("maze", 5)
local curAgency = doerlib.Agency:new(doerlib.AgencyType.CONTROLLER)
curAgency:configure()
curAgency:registerAgent(mazeAgent)

return {start = function() curAgency:run() end}