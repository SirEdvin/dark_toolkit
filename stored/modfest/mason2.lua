-- dependency
-- doerlib

local doerlib = require("doerlib")
local logger = require("core.logging")
local bus = require("core.bus")

local _log = logger.create("mason1")

local MasonAgent = doerlib.BaseAgent:new()
MasonAgent.peripheral = {}

function MasonAgent:new(name, loop_delay, o)
    o = o or doerlib.BaseAgent:new(name, loop_delay, o)
    setmetatable(o, self)
    self.__index = self
    return o
end

function MasonAgent:configure()
    self.peripheral = peripheral.find("masonAutomata")
    if self.peripheral == nil then
        error("Cannot find mason automata")
    end
    self.peripheral.setFuelConsumptionRate(6)
end

function MasonAgent:perform()
    local something, info = turtle.inspect()
    if something then
        if info.name == "minecraft:stone_bricks" then
            local chisel, err = self.peripheral.chisel("block", "minecraft:chiseled_stone_bricks")
            if not chisel then
                _log:error("Cannot chisel block", err)
            else
                bus.queueEvent("pusher", "factory1", {chain="mason2", ready=true}, true)
            end
        elseif info.name == "minecraft:chiseled_stone_bricks" then
            bus.queueEvent("pusher", "factory1", {chain="mason2", ready=true}, true)
        end
    end
end

local smithingAgent = MasonAgent:new(nil, 1)
local curAgency = doerlib.Agency:new(doerlib.AgencyType.CONTROLLER)
curAgency:configure()
curAgency:registerAgent(smithingAgent)

return {start = function() curAgency:run() end}