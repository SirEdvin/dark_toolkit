local pipe_x_anchors = {
    {x = 0, y=1, z=-2},
    {x = 0, y=1, z=-1},
    {x = 0, y=1, z=0},
    {x = 0, y=1, z=1},
    {x = 0, y=1, z=2},
}

local pipe_y_anchors = {
    {x = 0, y=2, z=0},
    {x=0, y=3, z=0}
}

local w1_anchors = {
    {x = 0, y=3, z=2},
    {x = 0, y=3, z=-1},
}

local w2_anchors = {
    {x = 0, y=3, z=1},
    {x = 0, y=3, z=-2},
}

local w3_anchors = {
    {x = 0, y=2, z=2},
    {x = 0, y=2, z=-1},
}

local w4_anchors = {
    {x = 0, y=2, z=1},
    {x = 0, y=2, z=-2},
}

local pipe_x_block = {block = "pswg:imperial_cutout_pipes", attrs={axis="x"}}
local pipe_y_block = {block = "pswg:imperial_cutout_pipes", attrs={axis="y"}}
local w1_block = {block="pswg:magenta_stained_imperial_glass", attrs={down=true, west=true, east=false, up=false, north=false, south=false}}
local w2_block = {block="pswg:magenta_stained_imperial_glass", attrs={down=true, west=false, east=true, up=false, north=false, south=false}}
local w3_block = {block="pswg:magenta_stained_imperial_glass", attrs={down=false, west=true, east=false, up=true, north=false, south=false}}
local w4_block = {block="pswg:magenta_stained_imperial_glass", attrs={down=false, west=false, east=true, up=true, north=false, south=false}}

local off_options = {playerPassable=false, invisible=false}
local on_options = {playerPassable=true, invisible=true}


local function apply_options(forger, options)
    forger.batchForgeRealityPieces({
        {pipe_x_anchors, pipe_x_block, options},
        {pipe_y_anchors, pipe_y_block, options},
        {w1_anchors, w1_block, options},
        {w2_anchors, w2_block, options},
        {w3_anchors, w3_block, options},
        {w4_anchors, w4_block, options}
    })
end

local function start()
    local scanner = peripheral.find("universal_scanner")
    local forger = peripheral.find("reality_forger")
    local sir_found = false
    apply_options(forger, off_options)
    while true do
        local scan, err = scanner.scan("player", 1)
        if err then
            print("Scanner error: " .. err)
            sleep(2.1)
        else
            sir_found = false
            for _, player in ipairs(scan) do
                if player.displayName == "SirEdvin" then
                    print("SirEdvin found, open the gate!")
                    apply_options(forger, on_options)
                    sir_found = true
                    sleep(6)
                end
            end
            if not sir_found then
                print("SirEdvin not found, close the gate!")
                apply_options(forger, off_options)
                sleep(2.1)
            end
        end
    end
end

return {
    start = start
}