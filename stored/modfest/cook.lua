local function findChest(chest)
    local chestFound = false
    for _ = 1, 4 do
        local present, blockData = turtle.inspect()
        if present and blockData.name == chest then
            chestFound = true
            break
        end
        turtle.turnLeft()
    end
    if not chestFound then
        return false, "cannot find chest"
    end
    return true
end

local function start()
    while true do
        findChest("minecraft:chest")
        local suck = turtle.suck(64)
        local first_suck = suck
        while suck do
            suck = turtle.suck(64)
        end
        if first_suck then
            turtle.turnLeft()
            turtle.turnLeft()
            for slot=1,16 do
                local item_count = turtle.getItemCount(slot)
                if item_count > 0 then
                    turtle.select(slot)
                    local result = turtle.drop(64)
                    while not result do
                        os.sleep(5)
                        print("Retry dropping")
                        result = turtle.drop(64)
                    end
                end
            end
        end
        os.sleep(10)
    end
end

return {
    start = start
}