-- dependency
-- libs:png
-- libs:statue
-- libs:player_skin
-- libs:base64
-- libs:tHelpers

local tHelpers = require("libs.tHelpers")
local statue = require("libs.statue")
local player_skin = require("libs.player_skin")
local pretty = require("cc.pretty")

local target_player = nil
local turtleMoving = false

local function ensure_statue_ready(statue, creative_chest)
    if not statue.isPresent() and not turtleMoving then
        turtleMoving = true
        turtle.up()
        turtle.turnLeft()
        turtle.turnLeft()
        creative_chest.generate("peripheralworks:flexible_statue", 1)
        turtle.place()
        turtle.turnLeft()
        turtle.turnLeft()
        turtle.down()
        turtleMoving = false
    end
end

local function print_player_head(player_name, statue_workbench)
    local result, value = player_skin.getSkinByName(player_name)
    if not result then
        print(value)
    else
        local cubes = statue.wrapPlayerHead(value)
		if statue_workbench ~= nil then
			if not statue_workbench.isPresent() then
				print("Statue is not present!")
			else
				statue_workbench.setCubes(cubes)
			end
		else
			print("Cannot find statue workbench")
			pretty.pretty_print(cubes)
		end
    end
end

local function collect_player_head(bow, chest)
    while true do
        if not turtleMoving then
            turtle.up()
            turtle.turnLeft()
            turtle.turnLeft()
            local has_block, data = turtle.inspect()
            if has_block and data.name == "peripheralworks:flexible_statue" then
                turtle.dig()
                turtle.suck()
                turtle.select(2)
                chest.generate("peripheralworks:flexible_statue", 1)
                turtle.place()
                turtle.select(1)
            end
            turtle.turnLeft()
            turtle.turnLeft()
            bow.shoot(4, 1)
            turtle.down()
            break
        else
            sleep(0.1)
        end
    end
end

local function waiting_for_player()
    local scanner = peripheral.find("universal_scanner")
    while true do
        local event, old_state, new_state = os.pullEvent("block_change")
        print("Get block changed event")
        if new_state.properties.powered and target_player == nil then
            local result, err = scanner.scan("player", 4)
            if result == nil then
                print("Scanning problem " .. err)
            else
                for _, player in ipairs(result) do
                    if player.x == 2 and player.z == 0 and (player.y == 0 or player.y == 1) then
                        print("Found player to print head! His name is " .. player.displayName)
                        target_player = player.displayName
                    end
                end
            end
        end
    end
end

local function main_loop()
    local statue = peripheral.find("statue_workbench")
    local chest = peripheral.find("creative_chest")
    local bow = peripheral.find("bow")
    while true do
        tHelpers.ensure_fuel_level()
        ensure_statue_ready(statue, chest)
        if target_player ~= nil then
            print_player_head(target_player, statue)
            statue.setStatueName(target_player .. "'s head")
            collect_player_head(bow, chest)
            target_player = nil
        end
        sleep(2.2)
    end
end

local function start()
    parallel.waitForAll(
        main_loop, waiting_for_player
    )
end

return {
    start = start
}