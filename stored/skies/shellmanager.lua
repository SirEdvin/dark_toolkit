local logging = require("core.logging")
local bus = require("core.bus")
local _log = logging.create("shellmanager")

local function shellManager()
    local p = peripheral.find("rsBridge")
    while true do
        local items = p.listItems()
        local shellCount = 0
        for _, item in pairs(items) do
            if item.name == "minecraft:shulker_shell" then
                shellCount = shellCount + item.amount
            end
        end
        _log:info("Amount of shells in storage: " .. shellCount)
        if shellCount < 200 then
            _log:info("Enabling shells production")
            bus.broadcastEvent(bus.EventType.SPAWNER_INFO_MESSAGE, {name = "shulker", priority = 100, enabled = true})
            redstone.setOutput("left", true)
        elseif shellCount > 1000 then
            _log:info("Disabling shells production")
            bus.broadcastEvent(bus.EventType.SPAWNER_INFO_MESSAGE, {name = "shulker", priority = 100, enabled = false})
            redstone.setOutput("left", false)
        else
            _log:info("Broadcasting current redstone output")
            bus.broadcastEvent(bus.EventType.SPAWNER_INFO_MESSAGE, {name = "shulker", priority = 100, enabled = redstone.getOutput("left")})
        end
        sleep(15)
    end
end

local function startBus()
    local eventBus = bus.EventBus:new(os.getComputerLabel(), true)
    eventBus:start()
end

local function start()
    parallel.waitForAll(shellManager, startBus)
end

return {
    start = start
}