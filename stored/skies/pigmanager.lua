local logging = require("core.logging")
local bus = require("core.bus")
local _log = logging.create("pigmanager")

local function pigManager()
    redstone.setOutput("left", false)
    while true do
        local _, message = os.pullEvent(bus.EventType.SPAWNER_INFO_MESSAGE)
        if message.enabled then
            _log:info("Disabling pig spawner, because shulker enabled")
            redstone.setOutput("left", false)
        else
            _log:info("Enabling pig spawner, because shulker disabled")
            redstone.setOutput("left", true)
        end
    end
end

local function startBus()
    local eventBus = bus.EventBus:new(os.getComputerLabel(), true)
    eventBus:listenEvent(bus.EventType.SPAWNER_INFO_MESSAGE)
    eventBus:start()
end

local function start()
    parallel.waitForAll(pigManager, startBus)
end

return {
    start = start
}