local function start()
    local p = peripheral.find("realityForger")
    while true do
        p.forgeReality({block = "minecraft:oak_log", playerPassable=true})
        sleep(5)
        p.forgeReality({block = "minecraft:nether_gold_ore", playerPassable=false})
        sleep(5)
    end
end

return {
    start = start
}