local expect = require("cc.expect").expect

local OS_BUILD = "unknown"
local _true_version_file_path = "/dark_toolkit/__version__.lua"

local function fread(file)
    expect(1, file, "string")
    local f = fs.open(file, "r")
    local data = f.readAll()
    f.close()
    return data
end

local function loadVersion()
    if fs.exists(_true_version_file_path) then
        local loaded_version = textutils.unserialise(fread(_true_version_file_path))
        OS_BUILD = loaded_version.commit_id
    end
end

loadVersion()

return {
    OS_VERSION = "0.1.0",
    OS_BUILD = OS_BUILD
}