local logging = require("core.logging")

local lib_models = require("doerlib.models")
local lib_constants = require("doerlib.contsants")

local _log = logging.create("doerlib.agent")

local BaseAgent = {
    name = "", state = lib_models.AgentState.INITIAL, configured = false,
    loop_delay = lib_constants.DEFAULT_AGENT_LOOP_DELAY, _targetEvents = nil,
    _eventHandlers = {}
}

function BaseAgent:new(name, loop_delay, o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    o.name = name or os.getComputerLabel()
    o.loop_delay = loop_delay or o.loop_delay
    return o
end

function BaseAgent:start()
    if not self.configured then
        self:configure()
        self.configured = true
    end
    _log:info("Starting agent " .. self.name)
    self:run()
    self:cleanup()
    _log:warn("Agent", self.name, "fully stoped")
end

function BaseAgent:configure()
    error("This method should be overrided")
end

function BaseAgent:perform()
    error("This method should be overrided")
end

function BaseAgent:_eventListener()
    if self._targetEvents == nil then
        error("How is this even possible")
    end
    local eventFilter = {}
    for _, targetEvent in pairs(self._targetEvents) do
        eventFilter[targetEvent] = true
    end
    while self.state == lib_models.AgentState.WORKING do
        local event = table.pack(os.pullEvent())
        if eventFilter[event[1]] and self._eventHandlers[event[1]] then
            self._eventHandlers[event[1]](table.unpack(event, 2))
        end
    end
end

function BaseAgent:_performLoop()
    while self.state == lib_models.AgentState.WORKING do
        self:perform()
        sleep(self.loop_delay)
    end
end

function BaseAgent:run()
    local coroutineToRun = {}
    if self.loop_delay > 0 then
        table.insert(coroutineToRun, function() self:_performLoop() end)
    end
    if self._targetEvents ~= nil then
        table.insert(coroutineToRun, function() self:_eventListener() end)
    end
    self.state = lib_models.AgentState.WORKING
    parallel.waitForAll(
        table.unpack(coroutineToRun)
    )
end

function BaseAgent:stop()
    self.state = lib_models.AgentState.SHUTTING_DOWN
    _log:warn("Stopping agent", self.name, "wait for loop finish, please")
end

function BaseAgent:cleanup()
    self.state = lib_models.AgentState.STOPPED
end


return {
    BaseAgent = BaseAgent,
}
