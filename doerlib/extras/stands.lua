local expect = require("cc.expect").expect
local pretty = require("cc.pretty")

local doerlib = require("doerlib")
local bus = require("core.bus")
local logging = require("core.logging")

local _log = logging.create("stand")

-- Scan cooldown in milliseconds
local SCAN_COOLDOWN = 2000
-- Delay for determining shooting result in seconds
local SHOOTING_WAIT = 3.5

local StandAgent = doerlib.BaseAgent:new()
StandAgent.stands = {}
StandAgent.scanners = {}
StandAgent.link = {}
StandAgent.cooldowns = {}
StandAgent.items = {}
StandAgent.reverse_items = {}
StandAgent.timers = {}
StandAgent.turtles = {}
StandAgent.players = {}
StandAgent.item_count = {}
StandAgent.cleanup_lock = false

function StandAgent:new(name, items, turtles, o)
    expect(1, name, "string")
    expect(2, items, "table")
    expect(3, turtles, "table")
    o = o or doerlib.BaseAgent:new(name, 60, o)
    setmetatable(o, self)
    self.__index = self
    self.items = items
    self.turtles = turtles
    self.reverse_items = {}
    for index, item in ipairs(items) do
        self.reverse_items[item] = index
    end
    return o
end

function StandAgent:initial_setup()
    for index, stand in ipairs(self.stands) do
        stand.setItem(self.items[index], "System is booting up ...")
    end
    self.item_count = self:refresh_item_count()
end

function StandAgent:refresh_item_count()
    local new_item_count = {}
    for _, item in ipairs(self.link.list()) do
        if new_item_count[item.name] == nil then
            new_item_count[item.name] = item.count
        else
            new_item_count[item.name] = new_item_count[item.name] + item.count
        end
    end
    return new_item_count
end

function StandAgent:disable_stand(stand_index)
    self.stands[stand_index].setItem(self.items[stand_index], "Please, wait a little more before using")
end

function StandAgent:mark_stand(stand_index, player_name)
    self.stands[stand_index].setItem(self.items[stand_index], "Player " .. player_name .. " shoots!")
    self.players[stand_index] = player_name
end


function StandAgent:enable_stand(stand_index)
    self.stands[stand_index].setItem(self.items[stand_index], "Right-click to shoot item!")
end

function StandAgent:handle_right_click(stand_index)
    local current_time = os.epoch("utc")
    local cooldown = self.cooldowns[stand_index] or 0
    if cooldown > current_time then
        self:disable_stand(stand_index)
    else
        local result, err = self.scanners[stand_index].scan("player")
        if result == nil then
            _log:warn("Problem with scanning ", err)
        else
            local target_player = nil
            for _, player in ipairs(result) do
                if (player.x == 2 or player.x == 3) and player.z == 0 and (player.y == 1 or player.y == 2) then
                    _log:info("Found player to to shoot! His name is ", player.displayName)
                    target_player = player.displayName
                end
            end
            if target_player == nil then
                _log:info("Unable to find player")
            else
                self:mark_stand(stand_index, target_player)
                local target_turtle = self.turtles[self.items[stand_index]]
                if target_turtle == nil then
                    _log:warn("Cannot find turtle for stand", stand_index)
                    self:disable_stand(stand_index)
                else
                    bus.queueEvent(target_turtle, "shoot_item", true)
                end
            end
            self.cooldowns[stand_index] = os.epoch("utc") + SCAN_COOLDOWN
            self:disable_stand(stand_index)
            self.timers[os.startTimer(SHOOTING_WAIT)] = stand_index
        end
    end
end

function StandAgent:handle_timer(timer_index)
    local stand_index = self.timers[timer_index]
    if stand_index ~= nil then
        self.timers[timer_index] = nil
        local target_player = self.players[stand_index]
        if target_player == nil then
            _log:warn("For some reason cannot actually check player")
        else
            local item = self.items[stand_index]
            local new_item_count = self:refresh_item_count()
            local item_count_diff = (new_item_count[item] or 0) - (self.item_count[item] or 0)
            if item_count_diff > 0 then
                bus.queueEvent("controller", "logging", "Player " .. target_player .. " hit the target", true)
                bus.queueLocalEvent("leaderboard_score", {player_name = target_player, score_amount = 1})
            else
                bus.queueEvent("controller", "logging", "Player " .. target_player .. " missed", true)
            end
        end
        self:enable_stand(stand_index)
    end
end

function StandAgent:configure()
    -- So, this is an issue with unsorted peripheral from my modem, which is problematic, but heh
    self.stands = {}
    self.scanners = {}
    for i = 1,#self.items do
        local new_stand_name = "display_pedestal_" .. i
        local new_stand = peripheral.wrap(new_stand_name)
        if new_stand == nil then
            error("Cannot find stand " .. new_stand_name)
        end
        table.insert(self.stands, new_stand)

        local new_scanner_name = "universal_scanner_" .. i
        local new_scanner = peripheral.wrap(new_scanner_name)
        if new_scanner == nil then
            error("Cannot find scanner " .. new_scanner_name)
        end
        table.insert(self.scanners, new_scanner)
    end
    self.link = peripheral.find("entity_link")
    self:initial_setup()
    self._targetEvents = {"pedestal_right_click", "timer"}
    self._eventHandlers = {
        pedestal_right_click = function(_, stand_hand)
            if not self.cleanup_lock then
                _log:info("Right click for pedestal", stand_hand.name, self.reverse_items[stand_hand.name])
                self:handle_right_click(self.reverse_items[stand_hand.name])
            end
        end,
        timer = function(timer_index) self:handle_timer(timer_index) end
    }
    for index, _ in ipairs(self.stands) do
        self:enable_stand(index)
    end
end

function StandAgent:perform()
    self.cleanup_lock = true
    for index, _ in ipairs(self.stands) do
        self:disable_stand(index)
    end
    for index, item in ipairs(self.link.list()) do
        self.link.pushItems("turtle_1", index)
    end
    for index, _ in ipairs(self.stands) do
        self:enable_stand(index)
    end
    self.cleanup_lock = false
end

-- function StandAgent:cleanup()
--     doerlib.BaseAgent.cleanup(self)
--     for index, _ in ipairs(self.stands) do
--         self:disable_stand(index)
--     end
-- end

return {
    StandAgent = StandAgent
}