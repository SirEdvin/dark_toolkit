local expect = require("cc.expect").expect
--local pretty = require("cc.pretty")

local doerlib = require("doerlib")
local bus = require("core.bus")
local logging = require("core.logging")

local _log = logging.create("stand")

local LeaderboardAgent = doerlib.BaseAgent:new()
LeaderboardAgent.file_name = ""
LeaderboardAgent.score_table = {}

function LeaderboardAgent:new(name, file_name, o)
    expect(1, name, "string")
    expect(2, file_name, "string", "nil")
    o = o or doerlib.BaseAgent:new(name, 5, o)
    setmetatable(o, self)
    self.__index = self
    self.file_name = file_name or name .. ".score"
    return o
end

function LeaderboardAgent:save()
    local opened_file = fs.open(self.file_name, "w")
    opened_file.write(textutils.serialise(self.score_table, {compact=true}))
    opened_file.close()
end

function LeaderboardAgent:load()
    local opened_file = fs.open(self.file_name, "r")
    if opened_file ~= nil then
        local raw_data = opened_file.readAll()
        self.score_table = textutils.unserialise(raw_data)
        opened_file.close()
    else
        self.score_table = {}
    end
end

function LeaderboardAgent:add_new_score(player_name, score_amount)
    if self.score_table[player_name] == nil then
        self.score_table[player_name] = score_amount
    else
        self.score_table[player_name] = self.score_table[player_name] + score_amount
    end
    self:save()
    self:draw()
end

function LeaderboardAgent:draw()
    -- Static
    local mon = self.monitor
    local w, h = mon.getSize()

    local tableWidth = math.floor( (w - 4*3) / 2 )
    local firstTblOff, tblHeight = 4, h-5
    local secondTblOff = 4*2 + tableWidth

    -- Sort scoreboard
    local sorted = {}
    for player, score in pairs(self.score_table) do
        table.insert(sorted, {name=player, score=score})
    end

    table.sort(sorted, function(a, b)
        return a.score > b.score
    end)

    -- Clear
    mon.setBackgroundColor(colors.black)
    mon.clear()

    -- Title
    mon.setTextColor(colors.green)
    mon.setCursorPos(math.floor(w/2-#("-=-=- TOTAL SCORE RANKING -=-=-")/2), 1)
    mon.write("-=-=- TOTAL SCORE RANKING -=-=-")

    -- Description
    mon.setTextColor(colors.lightGray)
    mon.setCursorPos(math.floor(w/2-#("Who hit the target most times?")/2), h)
    mon.write("Who hit the target the most times?")

    mon.setTextColor(colors.gray)
    for y=3, h-2 do
        mon.setCursorPos(secondTblOff-2, y)
        mon.write("|")
    end

    -- Draw scoreboard
    local function subScores(off, start, last)
        -- Labels
        local rankPos, namePos, scorePos = off, off+5, off+tableWidth-4
        mon.setTextColor(colors.orange)

        mon.setCursorPos(rankPos, 3)
        mon.write("RANK")

        mon.setCursorPos(namePos, 3)
        mon.write("NAME")

        mon.setCursorPos(scorePos, 3)
        mon.write("SCORE")

        for i=1, tblHeight do
            -- Get entry
            local entry = sorted[start+i-1]
            if not entry or (i == tblHeight and last) then
                -- indicate end of scoreboard
                mon.setTextColor(colors.lightGray)
                mon.setCursorPos(rankPos, 3+i)
                mon.write("  -")
            else
                -- Rank
                mon.setTextColor(colors.white)

                -- (Finally) display all that stuff...
                mon.setCursorPos(rankPos, 3+i)
                mon.write(("%3d"):format(start+i-1))

                mon.setCursorPos(namePos, 3+i)
                if #entry.name > scorePos-namePos-1 then
                    -- Long name
                    mon.write(("%s"):format(entry.name:sub(1, scorePos-namePos-1)))
                    mon.setTextColor(colors.lightGray)
                    mon.write("\187")
                    mon.setTextColor(colors.white)
                else
                    mon.write(("%s"):format(entry.name))
                end

                local score = (entry.score > 999 and 999 or entry.score)
                mon.setCursorPos(scorePos+1, 3+i)
                mon.write(("%3d"):format(score))
            end
        end
    end

    -- Final scoreboard
    subScores(firstTblOff,  1,              false)
    subScores(secondTblOff, tblHeight+1,    true)

    --mon.setCursorPos(1, 2)
    --local current_y = 2
    -- TODO: add sorting by value here, because raiting sort is required
    --for player, score_count in pairs(self.score_table) do
    --    mon.write(player .. " hit target " .. score_count .. " times")
    --    current_y = current_y + 1
    --    mon.setCursorPos(1, current_y)
    --end
end

function LeaderboardAgent:perform()
    self:draw()
end

function LeaderboardAgent:configure()
    self:load()
    self.monitor = peripheral.find("monitor")
    self:draw()
    self._targetEvents = {"leaderboard_score"}
    self._eventHandlers = {
        leaderboard_score = function(message)
            self:add_new_score(message.player_name, message.score_amount)
        end,
    }
end

return {
    LeaderboardAgent = LeaderboardAgent
}