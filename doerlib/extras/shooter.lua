local expect = require("cc.expect").expect
local pretty = require("cc.pretty")

local doerlib = require("doerlib")
local bus = require("core.bus")
local logging = require("core.logging")

local _log = logging.create("stand")

local ShootAgent = doerlib.BaseAgent:new()
ShootAgent.item = ""
ShootAgent.chest = {}
ShootAgent.bow = {}

function ShootAgent:new(name, item, o)
    expect(1, name, "string")
    expect(2, item, "string")
    o = o or doerlib.BaseAgent:new(name, 0, o)
    setmetatable(o, self)
    self.__index = self
    self.item = item
    return o
end

function ShootAgent:shoot()
    self.bow.shoot(0.3, 1, true)
end

function ShootAgent:restock()
    local current_amount = turtle.getItemCount()
    if current_amount < 32 then
        self.chest.generate(self.item, 32)
    end
end

function ShootAgent:configure()
    self.chest = peripheral.find("creative_chest")
    self.bow = peripheral.find("bow")
    self:restock()
    self.bow.setAngle(25)
    self._targetEvents = {"shoot_item"}
    self._eventHandlers = {
        shoot_item = function()
            self:shoot()
        end,
    }
end

return {
    ShootAgent = ShootAgent
}