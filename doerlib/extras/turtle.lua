local doerlib = require("doerlib")
local logging = require("core.logging")

local _log = logging.create("agents.turtle")

local _DEFAULT_TURTLE_AGENT_LOOP_DELAY = 5

local TurtleAgent = doerlib.BaseAgent:new()
TurtleAgent.brain = nil

function TurtleAgent:new(brain, name, loop_delay, o)
    loop_delay = loop_delay or _DEFAULT_TURTLE_AGENT_LOOP_DELAY
    o = o or doerlib.BaseAgent:new(name, loop_delay, o)
    setmetatable(o, self)
    self.__index = self
    o.brain = brain
    return o
end

function TurtleAgent:configure()
    self.brain:configure()
end

function TurtleAgent:perform()
    local result, err = self.brain:perform()
    if not result then
        _log:error(err)
    end
end

function TurtleAgent:cleanup()
    doerlib.BaseAgent.cleanup(self)
    self.brain:cleanup()
end

return {
    TurtleAgent = TurtleAgent,
}
