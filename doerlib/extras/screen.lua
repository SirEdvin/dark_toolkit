local doerlib = require("doerlib")
local ui = require("ui")
local bus = require("core.bus")

local ScreenAgent = doerlib.BaseAgent:new()
ScreenAgent.displays = {}
ScreenAgent.sideToNameMap = {}

function ScreenAgent:new(name, o)
    o = o or doerlib.BaseAgent:new(name, 0, o)
    setmetatable(o, self)
    self.__index = self
    o.displays = {}
    o.sideToNameMap = {}
    return o
end

function ScreenAgent:configure()
    self._targetEvents = {
        "monitor_touch",
        bus.EventType.DATA_CHANGED,
    }
    self._eventHandlers = {}
    self._eventHandlers["monitor_touch"] = function(side, x, y)
        local monitorName = self.sideToNameMap[side]
        if monitorName and self.displays[monitorName] then
            self.displays[monitorName]:handleClick(x, y)
        end
    end
    self._eventHandlers[bus.EventType.DATA_CHANGED] = function(eventData)
        if eventData.source then
            if self.displays[eventData.source] then
                self.displays[eventData.source]:draw()
            end
        end
        if eventData.monitor then
            if self.sideToNameMap[eventData.monitor] then
                self.displays[self.sideToNameMap[eventData.monitor]]:draw()
            end
        end
    end
end

function ScreenAgent:addMonitor(monitor, rootComponent, dataSourceName)
    self.displays[dataSourceName] = ui.Display:new(monitor, rootComponent)
    self.sideToNameMap[peripheral.getName(monitor)] = dataSourceName
end

function ScreenAgent:cleanup()
    doerlib.BaseAgent.cleanup(self)
    for _, display in pairs(self.displays) do
        display.context.monitor.clear()
    end
end

return {
    ScreenAgent = ScreenAgent
}
