local expect = require "cc.expect"

local _version = require("_version")

local AgentState = {
    INITIAL = "INITIAL",
    WORKING = "WORKING",
    SHUTTING_DOWN = "SHUTTING_DOWN",
    STOPPED = "STOPPED"
}


local AgencyControlType = {
    UPDATE = "UPDATE",
    REBOOT = "REBOOT"
}

local AgencyType = {
    CONTROLLER = "CONTROLLER",
    SLAVE = "SLAVE",
    SUPERVISOR = "SUPERVISOR",
}

local AgencyState = {
    INITIAL = "INITIAL",
    WORKING = "WORKING",
    UPDATING = "UPDATING",
    REBOOTING = "REBOOTING",
    STOPPING = "STOPPING",
    STOPPED = "STOPPED"
}

local _AgencyTypeIndices = {}
_AgencyTypeIndices[AgencyType.SUPERVISOR] = 100
_AgencyTypeIndices[AgencyType.CONTROLLER] = 10
_AgencyTypeIndices[AgencyType.SLAVE] = 1

function AgencyType.toindex(type)
    return _AgencyTypeIndices[type] or 0
end

local AgencyInfomation = {
    name = "",
    agents = {name1 = AgentState.INITIAL, name2 = AgentState.WORKING},
    build = "",
    version = "",
    state = "",
    networkName = "",
    type = "",
}

function AgencyInfomation:new(name, state, networkName, type, agents, o)
    expect.expect(1, name, "string")
    expect.expect(2, state, "string")
    expect.expect(3, networkName, "string")
    expect.expect(4, type, "string")
    expect.expect(5, agents, "table")
    expect.expect(6, o, "table", "nil")
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    o.name = name
    o.state = state
    o.networkName = networkName
    o.type = type
    o.agents = agents
    o.build = _version.OS_BUILD
    o.version = _version.OS_VERSION
    return o
end

function AgencyInfomation:extract(agency)
    local agentsData = {}
    for _, agent in pairs(agency.agents) do
        agentsData[agent.name] = agent.state
    end
    return AgencyInfomation:new(
        agency.name, agency.state, agency.bus.name,
        agency.type, agentsData
    )
end

return {
    -- states
    AgentState = AgentState,
    AgencyState = AgencyState,
    -- types
    AgencyControlType = AgencyControlType,
    AgencyType = AgencyType,
    -- shared data
    AgencyInfomation = AgencyInfomation,
}
