local bus = require("core.bus")
local logging = require("core.logging")
local updater = require("updater")

local lib_models = require("doerlib.models")
local lib_constants = require("doerlib.contsants")

local _log = logging.create("doerlib.agency")

local _INTENRAL_EVENTS = {
    STOP_EVENT = "STOP_EVENT"
}

local AgencyController = {}

function AgencyController._buildUpdateEvent()
    return {
        type = lib_models.AgencyControlType.UPDATE,
    }
end

function AgencyController._buildRebootEvent()
    return {
        type = lib_models.AgencyControlType.REBOOT,
    }
end

function AgencyController.updateAll()
    bus.broadcastEvent(
        bus.EventType.AGENCY_CONTROL_MESSAGE,
        AgencyController._buildUpdateEvent()
    )
end

function AgencyController.updateAgency(agencyName)
    if agencyName ~= nil then
        bus.queueEvent(
            agencyName, bus.EventType.AGENCY_CONTROL_MESSAGE,
            AgencyController._buildUpdateEvent()
        )
    end
end

function AgencyController.rebootAgency(agencyName)
    if agencyName ~= nil then
        bus.queueEvent(
            agencyName, bus.EventType.AGENCY_CONTROL_MESSAGE,
            AgencyController._buildRebootEvent()
        )
    end
end


local Agency = {
    agents = nil, bus = nil, name = nil, type = nil,
    state = lib_models.AgencyState.INITIAL, update_loop_delay = lib_constants.DEFAULT_UPDATE_LOOP_DELAY
}

function Agency:new(type, name, update_loop_delay, o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    o.type = type
    o.name = name or os.getComputerLabel()
    o.update_loop_delay = update_loop_delay or o.update_loop_delay
    o.agents = {}
    return o
end

function Agency:_buildExitHandler(waiting_delay)
    waiting_delay = waiting_delay or lib_constants.DEFAULT_EXIT_WAITING_DELAY
    return function()
        while self.state == lib_models.AgencyState.WORKING do
            local _, key = os.pullEvent("key")
            if key == keys.q then
                self:stopAgents()
                self:stopAgency()
            elseif key == keys.u then
                self:_updateToolkit()
            end
        end
        -- waiting for stopped
        self:waitAndStopBus(waiting_delay)
        _log:warn("Agency exit handler stopped")
    end
end

function Agency:sendInformation()
    local eventData = lib_models.AgencyInfomation:extract(self)
    self.bus:broadcastEvent(bus.EventType.AGENCY_INFORMATION_MESSAGE, eventData)
end

function Agency:_buildUpdateSender()
    return function()
        while self.state == lib_models.AgencyState.WORKING do
            self:sendInformation()
            sleep(self.update_loop_delay)
        end
        _log:warn("Agency update sender stopped")
    end
end

function Agency:_buildListener()
    return function()
        while self.state == lib_models.AgencyState.WORKING do
            local event = table.pack(os.pullEvent())
            if event[1] == bus.EventType.AGENCY_CONTROL_MESSAGE then
                local message = event[2]
                if message.type == lib_models.AgencyControlType.UPDATE then
                    self:_updateToolkit()
                elseif message.type == lib_models.AgencyControlType.REBOOT then
                    self:stopAgency(lib_models.AgencyState.REBOOTING)
                    self:stopAgents()
                    self:waitAndStopBus()
                    os.reboot()
                end
            elseif event[1] == _INTENRAL_EVENTS.STOP_EVENT then
                break
            end
        end
        _log:warn("Agency listener stopped")
    end
end

function Agency:_updateToolkit()
    local updateRequired = updater.toolkitUpdateRequired()
    if updateRequired then
        _log:info("Toolkit update scheduled")
        self:stopAgency(lib_models.AgencyState.UPDATING)
        self:stopAgents()
        self:waitAndStopBus()
        updater.updateDarkToolkit(true)
    else
        _log:info("Already on last version")
    end
end

function Agency:registerAgent(agent)
    table.insert(self.agents, agent)
    agent:configure()
    agent.configured = true
    if agent._targetEvents ~= nil then
        for _, event in pairs(agent._targetEvents) do
            self.bus:listenEvent(event)
        end
    end
end

function Agency:configure(listenedEvents)
    self.bus = bus.EventBus:new()
    if listenedEvents then
        for _, event in pairs(listenedEvents) do
            self.bus:listenEvent(event)
        end
    end
    self.bus:listenEvent(bus.EventType.AGENCY_CONTROL_MESSAGE)
end

function Agency:run()
    _log:info("Starting agency", self.name)
    self.state = lib_models.AgencyState.WORKING
    local tasks = {
        function() self.bus:start() end,
        self:_buildExitHandler(),
        self:_buildUpdateSender(),
        self:_buildListener(),
    }
    for _, agent in pairs(self.agents) do
        table.insert(tasks, function() agent:start() end)
    end
    parallel.waitForAll(
        table.unpack(tasks)
    )
    _log:warn("Fully stopping agency")
    if self.state == lib_models.AgencyState.STOPPING then
        self.state = lib_models.AgencyState.STOPPED
    end
    self:sendInformation()
end

function Agency:stopAgents()
    for _, agent in pairs(self.agents) do
        agent:stop()
    end
    self:sendInformation()
end

function Agency:waitAndStopBus(waiting_delay)
    waiting_delay = waiting_delay or lib_constants.DEFAULT_EXIT_WAITING_DELAY
    local allAgentStopped = false
    while not allAgentStopped do
        allAgentStopped = true
        for _, agent in pairs(self.agents) do
            if agent.state ~= lib_models.AgentState.STOPPED then
                allAgentStopped = false
            end
        end
        if not allAgentStopped then
            sleep(waiting_delay)
        end
    end
    self:sendInformation()
    _log:warn("All agents stopped! Stopping bus")
    self.bus:stop()
end

function Agency:stopAgency(next_state)
    self.state = next_state or lib_models.AgencyState.STOPPING
    self:sendInformation()
    bus.queueLocalEvent(_INTENRAL_EVENTS.STOP_EVENT, nil)
end

return {
    Agency = Agency,
    AgencyController = AgencyController,
}